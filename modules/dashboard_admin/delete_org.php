<!DOCTYPE html>
<html lang="en">

<head>

  

<?php  

require_once ($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/config.php');
require_once ($GLOBALS['app_root'].'/core_templates/headers.php');

 ?>

    <!-- Your custom styles 
    <link href="../../css/style.css" rel="stylesheet">
    <link href="../dpcell/style.css" rel="stylesheet">

    (optional) -->

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

    <style>
    @media screen and (min-width: 768px) {
        #fullHeightModalRight {
            top: 66px;
            left: auto;
            height: auto;
            bottom: auto;
            overflow: visible;
        }

        .modal-body {
            max-height: calc(100vh - 200px);
            overflow-y: auto;
        }
    }

    /* div.dataTables_wrapper
     {
        width:800px;
        margin: 25px 50px 25px 25px;
    } */
    </style>
</head>

<body onload="initialize()" class="hidden-sn mdb-skin">

    <!--Double navigation-->
    <header>
        <!-- Sidebar navigation -->
        <?php  require_once($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/core_templates/side_nav_bar.php'); ?>
        <!--/. Sidebar navigation -->


        <!-- Navbar -->
        <?php  require_once($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/core_templates/top_nav_bar.php'); ?>
        <!-- /.Navbar -->


    </header>
    <!--/.Double navigation-->

    <!--Main Layout-->
    <main>
        <div class="container-fluid">

 

        </div>



    </main>
    <!--Main Layout-->

    <!--Modal: Login / Register Form-->
    <?php  require_once($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/core_templates/login_modal.php'); ?>
    <!--Modal: Login / Register Form-->


    <!-- /Start your project here-->

    <!-- SCRIPTS -->
    <?php  require_once($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/core_templates/scripts.php'); ?>
    <!-- SCRIPTS -->

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js">
    </script>
    <script type="text/javascript" charset="utf8"
        src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8"
        src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" charset="utf8"
        src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js">
    </script>
    <script type="text/javascript" charset="utf8"
        src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" charset="utf8"
        src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" charset="utf8"
        src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" charset="utf8"
        src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>


    <script type="text/javascript" src="delete_org.js"></script>




</body>

</html>