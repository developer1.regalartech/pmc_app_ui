var uuids = uuid();
var dtable_columns = [
  { data: "uuids", title: "UUIDS", defaultContent: "<i>Not Set</i>" },
  { data: "reservation_no", title: "Reservation No", defaultContent: "<i>Not set</i>" },
  { data: "number_50_1", title: "Number_50_1", defaultContent: "<i>Not set</i>" },
  { data: "file_request_details", title: "File path", defaultContent: "<button>View</button>" },
  {data:"file_no",title:"Number",defaultContent:"<i>Not Set</i>"},
  { data: "request_details", title: "Request Details", defaultContent: "<i>Not set</i>" },
  { data: "date_50_1", title: "Date", defaultContent: "<button>View</button>" },
  //  { data: "file_1"  ,  title: "Proposal Notification File download", defaultContent: "<button>Download</button>"},
 // { data: "sec_50_1_b_50_2", title: "Section 50", defaultContent: "<i>Not set</i>" },
 // { data: "deletion_details", title: "Deletion Details", defaultContent: "<i>Not set</i>" },
 // { data: "number_50_2", title: "Number", defaultContent: "<i>Not set</i>" },
 // { data: "date_50_2", title: "Date", defaultContent: "<i>Not set</i>" },
 //  { data: "deletion_area", title: "Deletion Area", defaultContent: "<i>Not set</i>" },
 
];

var callback2 = function (response2) {
 console.log(response2);
 console.log(response2.content_type);
 switch (response2.content_type) {
  case "image/jpeg":
   console.log(response2.content_type);
   $("#file_1").empty();
   $('#centralModalSm').modal('show');
   $('#file_1').append('<img src="data:image/jpg;base64,' + response2.content + '" width="250px" height="100px" alt="image">');
    break;
  case "application/pdf":
  console.log(response2.content_type);
    $("#file_1").empty();
    $('#centralModalSm').modal('show');
       $('#file_1').append('<iframe src="data:application/pdf;base64,' + response2.content + '" width=550px" height="400px"   alt="file">');
    break;
 
}

}



function onLogin() {

  var dtable = $('#datatable').DataTable({

    "ajax": {
      beforeSend: beforeSends,
      contentType: 'application/json; charset=utf-8',
      type: 'GET',
      url: INSTANCE_URL + '/api/v2/pg/_table/dp.section_50_1',
      dataSrc: function (json) {
        dtable_data = json.resource;
        // console.log(dtable_data);
        return dtable_data;
      }
    },

    "columns": dtable_columns,

    initComplete: function () {

      // Setup - add a text input to each footer cell
      $('#datatable thead tr').clone(true).appendTo('#datatable thead');
      $('#datatable thead tr:eq(1) th').each(function (i) {
        var title = $(this).text();
        $(this).html('<input type="text" placeholder="Search ' + title + '" />');

        $('input', this).on('keyup change', function () {
          if (dtable.column(i).search() !== this.value) {
            dtable.column(i).search(this.value).draw();
          }
        });

      });
      dtable.destroy();// a fix as data is dynamic but reinitalze is not really supported

      dtable = $('#datatable').DataTable({
        // select: {
        //   style: 'single'
        // },

        orderCellsTop: true,
        fixedHeader: true,
        "scrollY": 300,
        "scrollX": 200,

        dom: 'Bfrtip',
        buttons: [
          'copy', 'csv', 'excel', 'pdf', 'print'
        ]
      });
      dtable.column(0).visible(false);
      $('#datatable tbody').on('click', 'button', function () {
            
        var data = dtable.row($(this).parents('tr')).data();  
      console.log(data[0]);
      var query = { content: true, is_base64: true, download: true, include_properties: true, content_type:true }

      $.regalartech.apis('GET', '/api/v2/files/'+data[0], query, callback2);
        
      });

    }
  });
}