
function onLogin()
{
    DrawSideNav();

    
  var dtable = $('#datatable').DataTable( {
  "ajax":
  {
    beforeSend:beforeSends,
    contentType: 'application/json; charset=utf-8',
    type : 'GET',
    url: INSTANCE_URL+'/api/v2/pg/_table/dp.reservations',
    dataSrc: "resource"
  },

  "columns": [
                //{ data: "uuids"  ,  "title": "Sr no" },
                { data: "zone"  ,  "title": "Zone" , "defaultContent": "<i>Not set</i>" },
                { data: "sector"  ,  "title": "Sectors", "defaultContent": "<i>Not set</i>" },
                { data: "sec_26_res"  ,  "title": "Sec 26 Reservation", "defaultContent": "<i>Not set</i>" },
                { data: "sec_26_area"  ,  "title": "Sec 26 Area", "defaultContent": "<i>Not set</i>" },
                { data: "sec_26_cts_fp_sur"  ,  "title": "CTS FP SUR", "defaultContent": "<i>Not set</i>" },
                { data: "sec_26_village"  ,  "title": "Village Name", "defaultContent": "<i>Not set</i>" },
                { data: "sec_28_mod_no"  ,  "title": "Sec 28-4 Mod no ", "defaultContent": "<i>Not set</i>" },
                { data: "sec_31_sm_ep"  ,  "title": "Sec 31 SM, EP no", "defaultContent": "<i>Not set</i>" },
                { data: "sec_31_final"  ,  "title": "Sec 31 final", "defaultContent": "<i>Not set</i>" },
                { data: "sec_31_sanctioned"  ,  "title": "Sec 31 Proposed", "defaultContent": "<i>Not set</i>" },
                { data: "remark"  ,  "title": "Remark", "defaultContent": "<i>Not set</i>" },
                { data: "arp_1"  ,  "title": "Developed or not", "defaultContent": "<i>Not set</i>" }

                
            ],

  initComplete: function () {

        // Setup - add a text input to each footer cell
        $('#datatable thead tr').clone(true).appendTo( '#datatable thead' );
        $('#datatable thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );

            $( 'input', this ).on( 'keyup change', function () {
                if ( dtable.column(i).search() !== this.value ) {
                    dtable.column(i).search( this.value ).draw();
                }
            } );
        } );
        dtable.destroy();// a fix as data is dynamic but reinitalze is not really supported

        dtable = $('#datatable').DataTable( {
            orderCellsTop: true,
            fixedHeader: true,
            "scrollY": 500,
        "scrollX": 200,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
           
        } );

  
        }

        

      });

}
