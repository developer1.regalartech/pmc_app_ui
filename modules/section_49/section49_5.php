



<!DOCTYPE html>
<html lang="en">

<head>


        <?php  

        require_once ($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/config.php');
        require_once ($GLOBALS['app_root'].'/core_templates/headers.php');

        ?>

  
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="../../core/css/addons/datatables.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
    <style>
        @media screen and (min-width: 768px) {
            #toast-container {
                z-index: 99;
            }
            #fullHeightModalRight {
                top: 66px;
                left: auto;
                height: auto;
                bottom: auto;
                overflow: visible;
            }
            .modal-body {
                max-height: calc(100vh - 200px);
                overflow-y: auto;
            }
        }

        table.dataTable thead th,
        table.dataTable thead td {
            padding: 7px 105px;
            border-bottom: 1px solid #243A51;
        }
    </style>
</head>

<body onload="initialize()" class="hidden-sn mdb-skin">
    <!--Double navigation-->
    <header>
            
        <?php  
                require_once ($GLOBALS['app_root'].'/core_templates/side_nav_bar.php'); ?>
                <!--/. Sidebar navigation -->
                <!-- Navbar -->
        <?php   require_once ($GLOBALS['app_root'].'/core_templates/top_nav_bar.php'); ?>


        </header>
    <!--/.Double navigation-->
    <!--Main Layout-->
    <main>
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card textfont">
                        <div class="card-header fontalign">
                            Section 49(5) IF Govt. fails to take decision on notice
                        </div>
                        <div class="card-body">
                            <!-- <h5 class="card-title">Special title treatment</h5> -->
                            <form class="text-center border border-light p-5">
                                <div class="card">
                                    <div class="card-body">
                                        <!-- <h4>Proposal</h4> -->
                                        <div>
                                                <label for="reservation_no">Reservation Number</label>
                                            <select id="reservation_no" class="js-example-data-array" style="width: 100%"></select>
                                        </div>
                                       <br>
                                        <div>
                                                <label for="inWord_Number">Inword Number</label>
                                            <select id="inWord_Number" class="js-example-data-array" style="width: 100%"></select>
                                        </div>

                                        <div>
                                            <label for="date">Inword Date</label>
                                            <input type="text" id="date" class="form-control mb-4" placeholder="Inword Date" disabled>
                                        </div>
                                        
                                        <div>
                                            <label for="date1">Time to take decision</label>
                                            </label>
                                            <input type="text" id="date1" class="form-control mb-4" placeholder="Time to take decision" disabled>
                                        </div> 
                                        <div>
                                            <label for="date2">Notice conformation</label>
                                            </label>
                                            <input type="text" id="date2" class="form-control mb-4" placeholder="Notice conformation"disabled>
                                        </div>


                                    </div>
                                </div>
                                <br>

                                <div class="form-group">
                                    <!-- <button type="button" class="btn btn-danger" id="btn_reject">Reject</button> -->
                                    <button type="button" class="btn btn-success" id="submit_btn"> Save </button>

                                </div>

                            </form>
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>

    </main>
    <!--Main Layout-->
       <!--Modal: Login / Register Form-->
       <?php  require_once($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/core_templates/login_modal.php'); ?>

    <!--Modal: Login / Register Form-->
    <!-- /Start your project here-->
    <!-- SCRIPTS -->
    <!-- JQuery -->
    <?php   require_once ($GLOBALS['app_root'].'/core_templates/scripts.php'); ?>
   
    <script type="text/javascript" src="section49_5.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript" src="index.js"></script>
    <!-- <script type="text/javascript" src="sliderbars.js"></script> -->
</body>

</html>