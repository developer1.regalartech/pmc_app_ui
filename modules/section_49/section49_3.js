var file_upload_counts = 0;
var file1_path;
var file2_path;
var uuids1 = uuid();
function onLogin() {
  // $.regalartech.apis('GET', "/api/v2/pg/_table/dp.section_49_3", null, callback1);
  $('.datepicker').pickadate();
  $('#reservation_no').select2({
    ajax: {
      url: INSTANCE_URL + "/api/v2/pg/_table/dp.reservations",
      data: function (params) {
        var query = {
          filter: "sec_26_res LIKE '%" + params.term + "%'",
          fields: "sec_26_res,uuids"
        }
        return query;
      },
      beforeSend: beforeSends,
      processResults: function (data) {
        var res = [];
        for (var i = 0; i < data.resource.length; i++) {
          item = {}
          item["id"] = data.resource[i].uuids;
          item["text"] = data.resource[i].sec_26_res;

          // selected_uuids = data.resource[i].uuids;

          res.push(item);

        }
        return {
          results: res
        };
      }
    }
  });
  $('#inWord_Number').select2(
    {

      ajax: {
        url: INSTANCE_URL + "/api/v2/pg/_table/dp.section_49_1",
        data: function (params) {
          var query1 = {
            filter: "(inword LIKE '%" + params.term + "%') AND (reservation_no ='" + $('#reservation_no').select2('data')[0].id + "')",

            fields: "inword, uuids"
          }
          return query1;
        },

        beforeSend: beforeSends,
        processResults: function (data) {

          var res = [];
          console.log()
          for (var i = 0; i < data.resource.length; i++) {
            item = {}
            item["id"] = data.resource[i].uuids;
            item["text"] = data.resource[i].inword;

            res.push(item);

          }
          return {
            results: res

          };
        }
      }

    });


  $("#submit_btn").click(function () {

    $.regalartech.uploadOneFile('file_1', 'pmc/sec_49/sec_49_3/', uuids1 + 'number_pmctosg', null, file_upoad_response1);
    //$.regalartech.uploadOneFile('file_2', 'pmc/sec_49/sec_49_3/', uuids1 + 'file_letter_sgtopmc', null,file_upoad_response2);
  });
  var file_upoad_response1 = function (file_response1) {
    console.log(file_response1.path);
    if (file_response1.path && file_response1.name && file_response1.type) {
      file_upload_counts = file_upload_counts + 1;
      file1_path = file_response1.path;
      if (file_upload_counts == 2) {
        console.log(file_upload_counts);
        postdata();
      }
    }
  }
  var file_upoad_response2 = function (file_response2) {
    console.log(file_response2.path);
    if (file_response2.path && file_response2.name && file_response2.type) {
      file_upload_counts = file_upload_counts + 1;
      file2_path = file_response2.path;
      if (file_upload_counts == 2) {
        console.log(file_upload_counts);
        postdata();
      }
    }
  }
  function postdata() {
    var data = $('#reservation_no').select2('data');
    var Inword_number = $('#inWord_Number').select2('data');
    post_body = {
      "resource": [
        {
          "uuids": uuids1,
          "reservation_no": data[0].id,
          "inword_no": Inword_number[0].id,
          "number_sgtopmc": $("#number").val(),
          //  "number_pmctosg": $("#number").val(),
          "date_sgtopmc": $("#date1").val(),
          "date_pmctosg": $("#date2").val(),
          "desc_pmctosg": $("#desc").val(),
          "number_pmctosg": file1_path,
          "file_letter_sgtopmc": file2_path


        }]
    };
    $.regalartech.apis('POST', "/api/v2/pg/_table/dp.section_49_3", post_body, post_response);
    $.regalartech.apis('GET', "/api/v2/pg/_table/dp.section_49_3", null, callback1);
  }

}



$('.datepicker').pickadate({
  onSelect: function () {
    var date = $(this).val();
    // var time = $('#time').val();
    alert(date);
    // $("#start").val(date + time.toString(' HH:mm').toString());

  }
});



$("#date1").change(function () {

  var date = $(this).val();
  // var time = $('#time').val();
  //     alert(date);
  console.log(date)

  var c = new Date(date)
  c.setMonth(c.getMonth() + 1)
  var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

  var d = c.getDate() + " " + months[c.getMonth()] + ", " + c.getFullYear();
  $('#dt').val(d)

  // $("#start").val(date + time.toString(' HH:mm').toString());


});
var callback1 = function (response) {
  console.log(response)
  // $('#date').val(response[0].date_sgtopmc);
}
$('#inWord_Number').change(function () {
  var query2 = {
    filter: "( inword LIKE '%" + $('#inWord_Number').select2('data')[0].text + "%') AND (reservation_no ='" + $('#reservation_no').select2('data')[0].id + "')",
    fields: "date"
  };
  $.regalartech.apis('GET', "/api/v2/pg/_table/dp.section_49_1", query2, callback23);
});

var callback23 = function (ress) {
  console.log(ress);
  $('#date').val(ress[0].date);
}

var post_response = function (post_res) {
  console.log(post_res);
  alert("Successfully Inserted Data!!!...")
  // alerts("success", "Data Sucessfully Inserted");
}