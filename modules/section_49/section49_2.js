var file_upload_counts = 0;
var uuids1 = uuid();
function onLogin() {
  $.regalartech.apis('GET', "/api/v2/pg/_table/dp.section_49_2", null, callback1);
  $('.datepicker').pickadate();
  $('#reservation_no').select2({
    ajax: {
      url: INSTANCE_URL + "/api/v2/pg/_table/dp.reservations",
      data: function (params) {
        var query = {
          filter: "sec_26_res LIKE '%" + params.term + "%'",
          fields: "sec_26_res,uuids"
        }
        return query;
      },
      beforeSend: beforeSends,
      processResults: function (data) {
        var res = [];
        for (var i = 0; i < data.resource.length; i++) {
          item = {}
          item["id"] = data.resource[i].uuids;
          item["text"] = data.resource[i].sec_26_res;

          // selected_uuids = data.resource[i].uuids;

          res.push(item);

        }
        return {
          results: res
        };
      }
    }
  });

  $('#inWord_Number').select2(
    {

      ajax: {
        url: INSTANCE_URL + "/api/v2/pg/_table/dp.section_49_1",
        data: function (params) {
          var query1 = {
            filter: "(inword LIKE '%" + params.term + "%') AND (reservation_no ='" + $('#reservation_no').select2('data')[0].id + "')",

            fields: "inword, uuids"
                  }
          return query1;
        },

        beforeSend: beforeSends,
        processResults: function (data) {

          var res = [];
          console.log()
          for (var i = 0; i < data.resource.length; i++) {
            item = {}
            item["id"] = data.resource[i].uuids;
            item["text"] = data.resource[i].inword;
          
            res.push(item);

          }
          return {
            results: res

          };
        }
      }

    });

  $("#submit_btn").click(function () {

    $.regalartech.uploadOneFile('file_1', 'pmc/sec_49/sec_49_2/', uuids1 + 'file_document_details',null, file_upoad_response);
   // $.regalartech.uploadOneFile('file_1', 'pmc/sec_49/sec_49_2/', uuids1 + 'file_document_details', file_upoad_response);

  });
  var file_upoad_response = function(file_response)
  {
    if (file_response.path && file_response.name && file_response.type) {
      file_upload_counts = file_upload_counts + 1;
      if(file_upload_counts == 1)
      {
       
        var data = $('#reservation_no').select2('data');
        var Inword_number = $('#inWord_Number').select2('data');

        console.log(file_response.path);
        post_body = {
          "resource": [
            {
              "uuids": uuids1,
              "reservation_no": data[0].id,
              "inword_no":Inword_number[0].id,
      
              "number": $("#number1").val(),
              "date_2": $("#date1").val(),
              "file_document_details":file_response.path
      
            }]
        };
        $.regalartech.apis('POST', "/api/v2/pg/_table/dp.section_49_2", post_body, post_response);
        $.regalartech.apis('GET', "/api/v2/pg/_table/dp.section_49_2", null, callback1);
      
      }
    }
  }

}

var callback1 = function (response) {
  console.log(response);
}
$('#inWord_Number').change(function () {
  var query2 = {
    filter: "( inword LIKE '%" + $('#inWord_Number').select2('data')[0].text + "%') AND (reservation_no ='" + $('#reservation_no').select2('data')[0].id + "')",
    fields: "date"
  };
  $.regalartech.apis('GET', "/api/v2/pg/_table/dp.section_49_1", query2, callback23);
});

var callback23 = function (ress) {
  console.log(ress);
  $('#date').val(ress[0].date);
}

var post_response = function (post_res) {
  console.log(post_res);
  alert("Successfully Inserted Data!!!...")
 // alerts("success", "Data Sucessfully Inserted");
}