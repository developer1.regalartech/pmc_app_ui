<!DOCTYPE html>
<html lang="en">

<head>


    <?php  

        require_once ($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/config.php');
        require_once ($GLOBALS['app_root'].'/core_templates/headers.php');

        ?>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="../../core/css/addons/datatables.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
    <style>
        @media screen and (min-width: 768px) {
            #toast-container {
                z-index: 99;
            }

            #fullHeightModalRight {
                top: 66px;
                left: auto;
                height: auto;
                bottom: auto;
                overflow: visible;
            }

            .modal-body {
                max-height: calc(100vh - 200px);
                overflow-y: auto;
            }
        }

        table.dataTable thead th,
        table.dataTable thead td {
            padding: 7px 105px;
            border-bottom: 1px solid #243A51;
        }
    </style>
</head>

<body onload="initialize()" class="hidden-sn mdb-skin">
    <!--Double navigation-->
    <header>

        <?php  
                    require_once ($GLOBALS['app_root'].'/core_templates/side_nav_bar.php'); ?>
        <!--/. Sidebar navigation -->
        <!-- Navbar -->
        <?php   require_once ($GLOBALS['app_root'].'/core_templates/top_nav_bar.php'); ?>


    </header>
    <!--/.Double navigation-->
    <!--Main Layout-->
    <main>
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card textfont">
                        <div class="card-header fontalign">
                            Section 49(3)
                        </div>
                        <div class="card-body">
                            <!-- <h5 class="card-title">Special title treatment</h5> -->
                            <form class="text-center border border-light p-5">
                                <div class="card">
                                    <div class="card-body">
                                        <!-- <h4>Proposal</h4> -->
                                        <div>
                                            <label for="reservation_no">Reservation Number</label>
                                            <select id="reservation_no" class="js-example-data-array"
                                                style="width: 100%"></select>
                                        </div>
                                        <br>
                                        <div>
                                            <label for="inward_Number">Inward Number</label>
                                            <select id="inward_Number" class="js-example-data-array"
                                                style="width: 100%"></select>
                                        </div>
                                        <br>
                                        <div>
                                            <label for="date">Inward Date</label>
                                            <input type="text" id="date" class="form-control mb-4"
                                                placeholder="Inward Date" disabled>
                                        </div>

                                        <h3>State Govt To PMC</h3>

                                        <div class="input-default-wrapper mt-3">
                                            <span class="input-group-text mb-3" id="input1">Letter</span>
                                            <input type="file" id="file_1" class="input-default-js">
                                            <label class="label-for-default-js rounded-right mb-3" for="file_1">
                                                <!-- <span class="span-choose-file">Choose file
                                                </span>
                                                <div class="float-right span-browse">Browse</div> -->
                                            </label>
                                        </div>
                                        <div>
                                            <label for="number">Number</label>

                                            <input type="text" id="number" class="form-control mb-4"
                                                placeholder="Number">
                                        </div>
                                        <div class="md-form">
                                            <label for="date1"> Date</label>
                                            <input placeholder="Date" type="text" id="date1"
                                                class="form-control datepicker">
                                        </div>
                                        <div>
                                            <label for="dt">PMC must reply to the State Govt. within this date</label>

                                            <input type="text" id="dt" class="form-control mb-4" placeholder="Date"
                                                disabled>
                                        </div>


                                        <h3>PMC to State Govt</h3>
                                        <div>
                                            <label for="desc">Short Description</label>

                                            <!-- Sidebar navigation -->
                                            <div id="slide-out" class="side-nav sn-bg-4">
                                                <ul class="custom-scrollbar">
                                                    <!-- Logo -->
                                                    <li>
                                                        <div class="logo-wrapper waves-light">
                                                            <a href="">
                                                                <img src="" class="img-fluid flex-center">
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <!--/. Logo -->

                                                    <li>
                                                        <ul class="collapsible collapsible-accordion">

                                                            <li>
                                                                <a class="collapsible-header waves-effect arrow-r">
                                                                    <i class="fa fa-chevron-right"></i>DP Cell
                                                                    <i class="fa fa-angle-down rotate-icon"></i>
                                                                </a>
                                                                <div class="collapsible-body">
                                                                    <ul>
                                                                        <li>
                                                                            <a href="#" class="waves-effect">Add</a>
                                                                        </li>

                                                                        <li>
                                                                            <a href="dpcell.html"
                                                                                class="waves-effect">View </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#" class="waves-effect">Edit</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#" class="waves-effect">Delete</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </li>

                                                            <li>
                                                                <a class="collapsible-header waves-effect arrow-r">
                                                                    <i class="fa fa-chevron-right"></i>Modification
                                                                    Details
                                                                    <i class="fa fa-angle-down rotate-icon"></i>
                                                                </a>
                                                                <div class="collapsible-body">
                                                                    <ul>
                                                                        <li>
                                                                            <a href="modificationDetails.html"
                                                                                class="waves-effect"> Add</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="modificationdetails_table.html"
                                                                                class="waves-effect"> View</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#" class="waves-effect">Edit</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="#" class="waves-effect">Delete</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <a class="collapsible-header waves-effect arrow-r">
                                                                    <i class="fa fa-chevron-right"></i>Building
                                                                    Permission Dept
                                                                    <i class="fa fa-angle-down rotate-icon"></i>
                                                                </a>
                                                                <div class="collapsible-body">
                                                                    <ul>
                                                                        <li>
                                                                            <a href="section49_1.html"
                                                                                class="waves-effect">Section49_1(Add)</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="section49_1_table.html"
                                                                                class="waves-effect">Section49_1(view)</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="section49_2.html"
                                                                                class="waves-effect">Section49_12Add)</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="section49_2_table.html"
                                                                                class="waves-effect">Section49_2(view)</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="section49_3.html"
                                                                                class="waves-effect">
                                                                                Section49_3(Add)</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="section49_3_table.html"
                                                                                class="waves-effect">
                                                                                Section49_3(view)</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="section49_4.html"
                                                                                class="waves-effect"> Section49_4(Add)
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="section49_4_table.html"
                                                                                class="waves-effect">
                                                                                Section49_4(view)</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="section49_5.html"
                                                                                class="waves-effect">Section49_5(Add)</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="section49_5_table.html"
                                                                                class="waves-effect">Section49_5view)</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="section49_7.html"
                                                                                class="waves-effect">
                                                                                Section49_7(Add)</a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="section49_7_table.html"
                                                                                class="waves-effect">Section49_7(view)</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="sidenav-bg mask-strong"></div>
                                        </div>
                                        <!--/. Sidebar navigation -->
                                        <!-- Navbar -->
                                        <nav id="reg-nav"
                                            class="navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav">
                                            <!-- SideNav slide-out button -->
                                            <div class="float-left">
                                                <a href="#" data-activates="slide-out" class="button-collapse">
                                                    <i class="fa fa-bars"></i>
                                                </a>
                                            </div>
                                            <!-- Breadcrumb-->
                                            <div class="breadcrumb-dn mr-auto lead">
                                                <strong id="project-title"> </strong>
                                            </div>
                                            <ul class="nav navbar-nav nav-flex-icons ml-auto">

                                                <li class="nav-item">
                                                    <a class="nav-link">
                                                        <i class="fa fa-comments-o"></i>
                                                        <span class="clearfix d-none d-sm-inline-block">Support</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link">
                                                        <i class="fa fa-user"></i>
                                                        <span class="clearfix d-none d-sm-inline-block">Account</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a id="reg-out" class="nav-link">
                                                        <i class="fa fa-sign-out"></i>
                                                        <span class="clearfix d-none d-sm-inline-block">Logout</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                        <!-- /.Navbar -->
                                        <div class="md-form">
                                            <label for="date2"> Date</label>
                                            <input placeholder="Date" type="text" id="date2"
                                                class="form-control datepicker">
                                        </div>

                                    </div>
                                </div>
                        </div>

                        <br>

                        <div class="form-group">
                            <!-- <button type="button" class="btn btn-danger" id="btn_reject">Reject</button> -->
                            <button type="button" class="btn btn-success" id="submit_btn"> Save </button>

                        </div>

                        </form>
                        <!-- </div> -->
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        </div>

    </main>
    <!--Main Layout-->
    <!--Modal: Login / Register Form-->
    <?php  require_once($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/core_templates/login_modal.php'); ?>

    <!--Modal: Login / Register Form-->
    <!-- /Start your project here-->
    <!-- SCRIPTS -->
    <!-- JQuery -->
    <?php   require_once ($GLOBALS['app_root'].'/core_templates/scripts.php'); ?>


    <script type="text/javascript" src="section49_3.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript" src="index.js"></script>
    <!-- <script type="text/javascript" src="sliderbars.js"></script> -->
</body>

</html>