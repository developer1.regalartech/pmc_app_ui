var uuids = uuid();

var dtable_columns = [
  { data: "reservation_no", title: "Reservation No", defaultContent: "<i>Not set</i>" },
  { data: "inward_no", title: "Inword No", defaultContent: "<i>Not set</i>" },
  
  { data: "time_decision", title: "Time To Take Decision", defaultContent: "<i>Not set</i>" },
  { data: "date_2", title: "Notice Conformation", defaultContent: "<i>Not set</i>" },
  //{ data: "short_desc", title: "Proposal Short Description", defaultContent: "<i>Not set</i>" },

];




function onLogin() {

  var dtable = $('#datatable').DataTable({

    "ajax": {
      beforeSend: beforeSends,
      contentType: 'application/json; charset=utf-8',
      type: 'GET',
      url: INSTANCE_URL + '/api/v2/pg/_table/dp.section_49_5',
      dataSrc: function (json) {
        dtable_data = json.resource;
        // console.log(dtable_data);
        return dtable_data;
      }
    },

    "columns": dtable_columns,

    initComplete: function () {

      // Setup - add a text input to each footer cell
      $('#datatable thead tr').clone(true).appendTo('#datatable thead');
      $('#datatable thead tr:eq(1) th').each(function (i) {
        var title = $(this).text();
        $(this).html('<input type="text" placeholder="Search ' + title + '" />');

        $('input', this).on('keyup change', function () {
          if (dtable.column(i).search() !== this.value) {
            dtable.column(i).search(this.value).draw();
          }
        });

      });
      dtable.destroy();// a fix as data is dynamic but reinitalze is not really supported

      dtable = $('#datatable').DataTable({
        // select: {
        //   style: 'single'
        // },

        orderCellsTop: true,
        fixedHeader: true,
        "scrollY": 300,
        "scrollX": 200,

        dom: 'Bfrtip',
        buttons: [
          'copy', 'csv', 'excel', 'pdf', 'print','colvis'
        ]
      });
     
    }
  });
}