





<!DOCTYPE html>
<html lang="en">

<head>
        <?php  

        require_once ($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/config.php');
        require_once ($GLOBALS['app_root'].'/core_templates/headers.php');

        ?>

    

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--
          <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
           <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> -->
    <!-- <link href="../../css/addons/datatables.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link href="style.css" rel="stylesheet">

    <style>
        @media screen and (min-width: 768px) {
            #toast-container {
                z-index: 99;
            }

            #fullHeightModalRight {
                top: 66px;
                left: auto;
                height: auto;
                bottom: auto;
                overflow: visible;
            }

            .modal-body {
                max-height: calc(100vh - 200px);
                overflow-y: auto;
            }
            /* .text-center {
          text-align: left!important;
      } */
        }

        table.dataTable thead th,
        table.dataTable thead td {
            padding: 7px 105px;
            border-bottom: 1px solid #243A51;
        }

        /* div.dataTables_wrapper
           {
              width:800px;
              margin: 25px 50px 25px 25px;
          } */
    </style>
</head>

<body onload="initialize()" class="hidden-sn mdb-skin">

    <!--Double navigation-->
    <header>
    <?php  
        require_once ($GLOBALS['app_root'].'/core_templates/side_nav_bar.php'); ?>
        <!--/. Sidebar navigation -->
        <!-- Navbar -->
    <?php   require_once ($GLOBALS['app_root'].'/core_templates/top_nav_bar.php'); ?>

            
        </header>
    <!--/.Double navigation-->
    <main>
        <div class="container-fluid">


          

            <div class="col-md-12">

                <!--Card-->
                <div class="card">
                    <Card Data>
                        <table id="datatable"> </table>
                    </Card>
                </div>
            </div>
        </div>
        </div>
    </main>
         <!--Modal: Login / Register Form-->
         <?php  require_once($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/core_templates/login_modal.php'); ?>

        <!--Modal: Login / Register Form-->


    <!-- /Start your project here-->

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <?php   require_once ($GLOBALS['app_root'].'/core_templates/scripts.php'); ?>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js">
    </script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>


    <script type="text/javascript" src="section49_5_table.js"></script>

    <script type="text/javascript" src="index.js"></script>






</body>

</html>