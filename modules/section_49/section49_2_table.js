var uuids = uuid();

var dtable_columns = [
   { data: "file_document_details", title: "path", defaultContent: "<i>Not set</i>" },
   { data: "reservation_no", title: "Reservation No", defaultContent: "<i>Not set</i>" },
   { data: "inword_no", title: "Inword No", defaultContent: "<i>Not set</i>" },
  { data: "file", title: "Document Details", defaultContent:"<button>View</button>" },
  { data: "number", title: "Number", defaultContent: "<i>Not set</i>" },
  { data: "date_2", title: " Date", defaultContent: "<i>Not set</i>" },
  
];
var callback27 = function (response12) {
  console.log(response12);
  console.log(response12.content_type);
  switch (response12.content_type) {
   case "image/jpeg":
    console.log(response12.content_type);
    $("#file_12").empty();
    $('#centralModalSm').modal('show');
    $('#file_12').append('<img src="data:image/jpg;base64,' + response12.content + '" width="250px" height="100px" alt="image">');
     break;
   case "application/pdf":
   console.log(response12.content_type);
     $("#file_12").empty();
     $('#centralModalSm').modal('show');
     //  $('#file_12').append('<object src="data:application/pdf;base64,' + response12.content + '"  alt="file">');
      $('#file_12').append('<iframe src="data:application/pdf;base64,' + response12.content + '" width=550px" height="400px"   alt="file">');
     break;
   // default:
   //   $('#reg_imgs').append('<img src="data:image/jpg;base64,' + image_data.content + '" width="750px" height="450px" class="img-fluid b-4" alt="Responsive image">');
 }
  
   //  }
 }
 
 
 
 function onLogin() {
 
   var dtable = $('#datatable').DataTable({
 
     "ajax": {
       beforeSend: beforeSends,
       contentType: 'application/json; charset=utf-8',
       type: 'GET',
       url: INSTANCE_URL + '/api/v2/pg/_table/dp.section_49_2',
       dataSrc: function (json) {
         dtable_data = json.resource;
         // console.log(dtable_data);
         return dtable_data;
       }
     },
 
     "columns": dtable_columns,
 
     initComplete: function () {
 
       // Setup - add a text input to each footer cell
       $('#datatable thead tr').clone(true).appendTo('#datatable thead');
       $('#datatable thead tr:eq(1) th').each(function (i) {
         var title = $(this).text();
         $(this).html('<input type="text" placeholder="Search ' + title + '" />');
 
         $('input', this).on('keyup change', function () {
           if (dtable.column(i).search() !== this.value) {
             dtable.column(i).search(this.value).draw();
           }
         });
 
       });
       dtable.destroy();// a fix as data is dynamic but reinitalze is not really supported
 
       dtable = $('#datatable').DataTable({
         // select: {
         //   style: 'single'
         // },
        
         orderCellsTop: true,
         fixedHeader: true,
         "scrollY": 300,
         "scrollX": 200,
 
         dom: 'Bfrtip',
         buttons: [
           'copy', 'csv', 'excel', 'pdf', 'print','colvis'
         ]
         
       });
       dtable.column(0).visible(false);
       $('#datatable tbody').on('click', 'button', function () {             
         var data = dtable.row($(this).parents('tr')).data();  
       console.log(data[0]);
       var query = { content: true, is_base64: true, download: true, include_properties: true, content_type:true }
 
       $.regalartech.apis('GET', '/api/v2/files/'+data[0], query, callback27);
         
       });
 
     }
   });
 }