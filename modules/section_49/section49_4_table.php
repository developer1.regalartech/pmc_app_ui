




<!DOCTYPE html>
<html lang="en">

<head>

        <?php  

        require_once ($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/config.php');
        require_once ($GLOBALS['app_root'].'/core_templates/headers.php');

        ?>
  
    <!-- <link href="css/style.css" rel="stylesheet"> -->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!--
          <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
           <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> -->
    <!-- <link href="../../css/addons/datatables.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link href="style.css" rel="stylesheet">

    <style>
        @media screen and (min-width: 768px) {
            #toast-container {
                z-index: 99;
            }

            #fullHeightModalRight {
                top: 66px;
                left: auto;
                height: auto;
                bottom: auto;
                overflow: visible;
            }

            .modal-body {
                max-height: calc(100vh - 200px);
                overflow-y: auto;
            }
            /* .text-center {
          text-align: left!important;
      } */
        }

        table.dataTable thead th,
        table.dataTable thead td {
            padding: 7px 105px;
            border-bottom: 1px solid #243A51;
        }

        /* div.dataTables_wrapper
           {
              width:800px;
              margin: 25px 50px 25px 25px;
          } */
    </style>
</head>

<body onload="initialize()" class="hidden-sn mdb-skin">

    <!--Double navigation-->
    <header>
            <?php  
                require_once ($GLOBALS['app_root'].'/core_templates/side_nav_bar.php'); ?>
                <!--/. Sidebar navigation -->
                <!-- Navbar -->
        <?php   require_once ($GLOBALS['app_root'].'/core_templates/top_nav_bar.php'); ?>



            
        </header>
    <!--/.Double navigation-->
    <main>
        <div class="container-fluid">


            <div class="modal fade" id="centralModalSm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                <!-- Change class .modal-sm to change the size of the modal -->
                <div class="modal-dialog modal-fluid" role="document">


                    <div class="modal-content">
                        <div class="modal-header">

                            <button type="button" class="close close_button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div id="file_1"></div>
                                        <div class="card-body">
                                            <!-- <h5 class="card-title">Special title treatment</h5> -->

                                        </div>
                                    </div>
                                </div>


                                <!-- Card body -->


                            </div>


                        </div>
                    </div>

                </div>

            </div>

            <div class="col-md-12">

                <!--Card-->
                <div class="card">
                    <Card Data>
                        <table id="datatable"> </table>
                    </Card>
                </div>
            </div>
        </div>
        </div>
    </main>
    <?php  require_once($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/core_templates/login_modal.php'); ?>


    <!-- /Start your project here-->

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <?php   require_once ($GLOBALS['app_root'].'/core_templates/scripts.php'); ?>

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js">
    </script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>


    <script type="text/javascript" src="section49_4_table.js"></script>

    <script type="text/javascript" src="index.js"></script>






</body>

</html>