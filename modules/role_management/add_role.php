<!DOCTYPE html>
<html lang="en">

<head>
    <?php  
require_once ($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/config.php');
require_once ($GLOBALS['app_root'].'/core_templates/headers.php'); 
?>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

    <style>
        @media screen and (min-width: 768px) {
            #fullHeightModalRight {
                top: 66px;
                left: auto;
                height: auto;
                bottom: auto;
                overflow: visible;
            }

            .modal-body {
                max-height: calc(100vh - 200px);
                overflow-y: auto;
            }
        }

        /* div.dataTables_wrapper
    {
    width:800px;
    margin: 25px 50px 25px 25px;
} */
    </style>
</head>

<body onload="initialize()" class="hidden-sn mdb-skin">

    <!--Double navigation-->
    <header>
        <!-- Sidebar navigation -->
        <?php  require_once ( $GLOBALS['app_root'].'/core_templates/side_nav_bar.php'); ?>
        <!--/. Sidebar navigation -->
        <!-- Navbar -->
        <?php  require_once ( $GLOBALS['app_root'].'/core_templates/top_nav_bar.php'); ?>
        <!-- /.Navbar -->
        <!--Modal: Login / Register Form-->
        <?php  require_once ($GLOBALS['app_root'].'/core_templates/login_modal.php'); ?>
        <!--Modal: Login / Register Form-->

    </header>
    <!--/.Double navigation-->

    <!--Main Layout-->
    <main>
        <div class="container-fluid col-sm-12 col-md-8 col-lg-6">
            <div class="card" style='margin:10px 10px'>
                <div class="card-header" style="text-align: center;"><i class="fa fa-user-o" aria-hidden="true"></i> Add
                    Role </div>
                <div class="card-body">
                    <form class='needs-validation'>
                        <div class='row'>
                            <div class='col-12'>
                                <div class="card">
                                    <div style="padding: 5px 5px;">
                                        <p style="text-align: center;margin: auto;">Role Name :
                                            <input type="text" name="Role_name" id="role_name" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <!-- Table  -->
                        <table class="table table-bordered">
                            <!-- Table head -->
                            <thead>
                                <tr>
                                    <th>Section</th>
                                    <th>Add</th>
                                    <th>Edit</th>
                                    <th>Remove</th>
                                    <th>View</th>
                                    <th>All</th>
                                </tr>
                            </thead>
                            <!-- Table head -->

                            <!-- Table body -->
                            <tbody>
                            <tr>
                                    <th scope="row" class='sections' id='Reservation'>
                                        Reservation
                                    </th>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Reservation-1">
                                            <label class="custom-control-label" for="Reservation-1"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Reservation-2">
                                            <label class="custom-control-label" for="Reservation-2"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Reservation-3">
                                            <label class="custom-control-label" for="Reservation-3"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Reservation-4" checked>
                                            <label class="custom-control-label" for="Reservation-4"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Reservation-5"
                                                onclick="disableOtherCheckbox(this)">
                                            <label class="custom-control-label" for="Reservation-5"> </label>
                                        </div>
                                    </td>
                                </tr>
                                <!-- <tr>
                                    <th scope="row" class='sections' id='DPcell'>
                                        DP Cell
                                    </th>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="DPcell-1">
                                            <label class="custom-control-label" for="DPcell-1"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="DPcell-2">
                                            <label class="custom-control-label" for="DPcell-2"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="DPcell-3">
                                            <label class="custom-control-label" for="DPcell-3"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="DPcell-4" checked>
                                            <label class="custom-control-label" for="DPcell-4"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="DPcell-5"
                                                onclick="disableOtherCheckbox(this)">
                                            <label class="custom-control-label" for="DPcell-5"> </label>
                                        </div>
                                    </td>
                                </tr> -->
                                <tr>
                                    <th scope="row" class='sections' id='Section37'>
                                        Section 37
                                    </th>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section37-1">
                                            <label class="custom-control-label" for="Section37-1"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section37-2">
                                            <label class="custom-control-label" for="Section37-2"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section37-3">
                                            <label class="custom-control-label" for="Section37-3"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section37-4" checked>
                                            <label class="custom-control-label" for="Section37-4"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section37-5"
                                                onclick="disableOtherCheckbox(this)">
                                            <label class="custom-control-label" for="Section37-5"> </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row" class='sections' id='Section15'>
                                        Section 15.10
                                    </th>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section15-1">
                                            <label class="custom-control-label" for="Section15-1"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section15-2">
                                            <label class="custom-control-label" for="Section15-2"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section15-3">
                                            <label class="custom-control-label" for="Section15-3"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section15-4" checked>
                                            <label class="custom-control-label" for="Section15-4"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section15-5"
                                                onclick="disableOtherCheckbox(this)">
                                            <label class="custom-control-label" for="Section15-5"> </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row" class='sections' id='Section49'>
                                        Section 49
                                    </th>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49-1">
                                            <label class="custom-control-label" for="Section49-1"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49-2">
                                            <label class="custom-control-label" for="Section49-2"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49-3">
                                            <label class="custom-control-label" for="Section49-3"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49-4" checked>
                                            <label class="custom-control-label" for="Section49-4"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49-5"
                                                onclick="disableOtherCheckbox(this)">
                                            <label class="custom-control-label" for="Section49-5"> </label>
                                        </div>
                                    </td>
                                </tr>
                                <th scope="row" class='sections' id='Section49.1'>
                                        Section49.1
                                    </th>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.1-1">
                                            <label class="custom-control-label" for="Section49.1-1"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.1-2">
                                            <label class="custom-control-label" for="Section49.1-2"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.1-3">
                                            <label class="custom-control-label" for="Section49.1-3"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.1-4" checked>
                                            <label class="custom-control-label" for="Section49.1-4"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.1-5"
                                                onclick="disableOtherCheckbox(this)">
                                            <label class="custom-control-label" for="Section49.1-5"> </label>
                                        </div>
                                    </td>
                                </tr>
                                <th scope="row" class='sections' id='Section49.2'>
                                        Section49.2
                                    </th>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.2-1">
                                            <label class="custom-control-label" for="Section49.2-1"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.2-2">
                                            <label class="custom-control-label" for="Section49.2-2"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.2-3">
                                            <label class="custom-control-label" for="Section49.2-3"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.2-4" checked>
                                            <label class="custom-control-label" for="Section49.2-4"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.2-5"
                                                onclick="disableOtherCheckbox(this)">
                                            <label class="custom-control-label" for="Section49.2-5"> </label>
                                        </div>
                                    </td>
                                </tr>
                                <th scope="row" class='sections' id='Section49.3'>
                                        Section49.3
                                    </th>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.3-1">
                                            <label class="custom-control-label" for="Section49.3-1"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.3-2">
                                            <label class="custom-control-label" for="Section49.3-2"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.3-3">
                                            <label class="custom-control-label" for="Section49.3-3"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.3-4" checked>
                                            <label class="custom-control-label" for="Section49.3-4"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.3-5"
                                                onclick="disableOtherCheckbox(this)">
                                            <label class="custom-control-label" for="Section49.3-5"> </label>
                                        </div>
                                    </td>
                                </tr>
                                <th scope="row" class='sections' id='Section49.4'>
                                        Section49.4
                                    </th>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.4-1">
                                            <label class="custom-control-label" for="Section49.4-1"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.4-2">
                                            <label class="custom-control-label" for="Section49.4-2"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.4-3">
                                            <label class="custom-control-label" for="Section49.4-3"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.4-4" checked>
                                            <label class="custom-control-label" for="Section49.4-4"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.4-5"
                                                onclick="disableOtherCheckbox(this)">
                                            <label class="custom-control-label" for="Section49.4-5"> </label>
                                        </div>
                                    </td>
                                </tr>
                                <th scope="row" class='sections' id='Section49.5'>
                                        Section49.5
                                    </th>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.5-1">
                                            <label class="custom-control-label" for="Section49.5-1"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.5-2">
                                            <label class="custom-control-label" for="Section49.5-2"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.5-3">
                                            <label class="custom-control-label" for="Section49.5-3"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.5-4" checked>
                                            <label class="custom-control-label" for="Section49.5-4"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.5-5"
                                                onclick="disableOtherCheckbox(this)">
                                            <label class="custom-control-label" for="Section49.5-5"> </label>
                                        </div>
                                    </td>
                                </tr>
                                <th scope="row" class='sections' id='Section49.7'>
                                        Section49.7
                                    </th>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.7-1">
                                            <label class="custom-control-label" for="Section49.7-1"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.7-2">
                                            <label class="custom-control-label" for="Section49.7-2"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.7-3">
                                            <label class="custom-control-label" for="Section49.7-3"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.7-4" checked>
                                            <label class="custom-control-label" for="Section49.7-4"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section49.7-5"
                                                onclick="disableOtherCheckbox(this)">
                                            <label class="custom-control-label" for="Section49.7-5"> </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row" class='sections' id='Section50'>
                                        Section 50
                                    </th>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section50-1">
                                            <label class="custom-control-label" for="Section50-1"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section50-2">
                                            <label class="custom-control-label" for="Section50-2"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section50-3">
                                            <label class="custom-control-label" for="Section50-3"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section50-4" checked>
                                            <label class="custom-control-label" for="Section50-4"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section50-5"
                                                onclick="disableOtherCheckbox(this)">
                                            <label class="custom-control-label" for="Section50-5"> </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row" class='sections' id='Section51'>
                                        Section 51
                                    </th>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section51-1">
                                            <label class="custom-control-label" for="Section51-1"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section51-2">
                                            <label class="custom-control-label" for="Section51-2"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section51-3">
                                            <label class="custom-control-label" for="Section51-3"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section51-4" checked>
                                            <label class="custom-control-label" for="Section51-4"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section51-5"
                                                onclick="disableOtherCheckbox(this)">
                                            <label class="custom-control-label" for="Section51-5"> </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row" class='sections' id='Section127'>
                                        Section 127
                                    </th>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section127-1">
                                            <label class="custom-control-label" for="Section127-1"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section127-2">
                                            <label class="custom-control-label" for="Section127-2"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section127-3">
                                            <label class="custom-control-label" for="Section127-3"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section127-4" checked>
                                            <label class="custom-control-label" for="Section127-4"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="Section127-5"
                                                onclick="disableOtherCheckbox(this)">
                                            <label class="custom-control-label" for="Section127-5"> </label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row" class='sections' id='netmodification'>
                                        Net Modification
                                    </th>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="netmodification-1">
                                            <label class="custom-control-label" for="netmodification-1"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="netmodification-2">
                                            <label class="custom-control-label" for="netmodification-2"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="netmodification-3">
                                            <label class="custom-control-label" for="netmodification-3"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="netmodification-4" checked>
                                            <label class="custom-control-label" for="netmodification-4"> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="netmodification-5"
                                                onclick="disableOtherCheckbox(this)">
                                            <label class="custom-control-label" for="netmodification-5"> </label>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                            <!-- Table body -->
                        </table>
                        <!-- Table  -->
                        <hr>
                        <div class='row'>
                            <div class=" col-12">
                                <div style='padding: 5px 5px;text-align: center;'>
                                    <button class='btn btn-success' type="submit"><i class="fa fa-user-o"
                                            aria-hidden="true"></i>
                                        Create Role</button>
                                </div>
                            </div>

                        </div>

                    </form>
                </div>


            </div>



    </main>
    <!--Main Layout-->
    <!-- /Start your project here-->

    <!-- SCRIPTS -->
    <?php  require_once ( $GLOBALS['app_root'].'/core_templates/scripts.php'); ?>
    <!-- SCRIPTS -->



    <script type="text/javascript" src="add_role.js"></script>




</body>

</html>