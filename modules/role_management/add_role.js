function onLogin() {

}



(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                event.preventDefault();
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                } else {
                    getAllclickedVal()
                    var role_det
                    role_det = {
                        "resource": [{
                            role_name: $('#role_name').val(),
                            role_data: JSON.stringify(allPermissionObj)
                        }]
                    };
                    $.regalartech.apis('POST', '/api/v2/pg/_table/dp.role_manager', role_det, null, role_details_result);

                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

var role_details_result = function (response) {
    if (response) {
        console.log(response);
        if (response.length == 1) {
            alerts("success", "Role added succesfully");
        }
        else {
            alerts("error", "Error");
        }
    }
    else {
        alerts("error", "Error adding role");
    }
};




var allPermissionObj = {}
// this function checks how many rows are there and then creates an array of all checked value for that 
//row e.g. DPcell, Section50,Section127,etc. 
function getAllclickedVal() {
    allPermissionObj = {}
    var allSections = document.getElementsByClassName('sections')
    for (i = 0; i < allSections.length; i++) {
        var id = allSections[i].id
        window[id + 'cell'] = []
        for (j = 1; j < 6; j++) {
            if (document.getElementById(id + '-' + j).checked) {
                window[id + 'cell'].push(j)
            }
        }
        allPermissionObj[id] = window[id + 'cell'].join()
    }

}


//this function disables all checkbox, if last one is clicked
function disableOtherCheckbox(checkbox) {
    var Ogid = checkbox.id.split('-')[0]
    if (checkbox.checked) {

        for (i = 1; i < 5; i++) {
            var theCheckbox = document.getElementById(Ogid + '-' + i)
            if (theCheckbox.checked) {
                theCheckbox.checked = false
            }
            $("#" + Ogid + '-' + i).prop("disabled", true);
        }
    } else {
        for (i = 1; i < 5; i++) {
            var theCheckbox = document.getElementById(Ogid + '-' + i)
            $("#" + Ogid + '-' + i).prop("disabled", false);
        }
    }



}