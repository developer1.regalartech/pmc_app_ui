var uuids = uuid();
var type ;
//var global_uuid;
//file_upload_counts = 0;
//file = file_prop_notification_no;
var dtable_columns = [
  { data: "file_1_notification_num", title: "File path1", defaultContent: "<i>Not Set</i>" },
  { data: "file_2_mod_map", title: "File path2", defaultContent: "<i>Not Set</i>" },
  { data: "reservation_no", title: "Reservation No", defaultContent: "<i>Not set</i>" },
 { data: "proposalnumber", title: "Proposal Notification Number", defaultContent: "<i>Not set</i>" },
 { data: "sec_37", title: "Proposal Section ", defaultContent: "<i>Not set</i>" },
 { data: "date", title: "Proposal Date", defaultContent: "<i>Not set</i>" },
 //{ data: "notification_num"  ,  title: "Final Decision Notification Number", defaultContent: "<i>Not set</i>"},
  //{ data: "final", title: "file_1_notification_num", defaultContent: "<button id=path1>View</button>"  },
  { data: "prop_date", title: "Final Date", defaultContent: "<i>Not set</i>" },
 { data: "short_desc", title: "Final Short Description", defaultContent: "<i>Not set</i>" },
 // { data: "final1", title: "file_2_mod_map", defaultContent: "<button id=path2>View</button>"  }, 
 {data:"change_area",title:"Change Area",defaultContent:"<i>Not Set</i>"}
];

var callback27 = function (response12) {
  console.log(response12);
  console.log(response12.content_type);
  switch (response12.content_type) {
   case "image/jpeg":
    console.log(response12.content_type);
     $("#file_2").empty();
     $("#file_1").empty();
   
    $('#centralModalSm').modal('show');

    $('#file_2').append('<img src="data:image/jpg;base64,' + response12.content + '" width="250px" height="100px" alt="image">');
     break;
   case "application/pdf":
   console.log(response12.content_type);
     $("#file_2").empty();
     $("#file_1").empty();
     $('#centralModalSm').modal('show');
  
      $('#file_2').append('<iframe src="data:application/pdf;base64,' + response12.content + '" width=550px" height="400px"   alt="file">');
    
     break;
   // default:
   //   $('#reg_imgs').append('<img src="data:image/jpg;base64,' + image_data.content + '" width="750px" height="450px" class="img-fluid b-4" alt="Responsive image">');
 }
 
  
   //  }
 }
 var callback28 = function (response28) {
  console.log(response28);
  console.log(response28.content_type);
  switch (response28.content_type) {
   case "image/jpeg":
    console.log(response28.content_type);
    $("#file_2").empty();
     $("#file_1").empty();
   
    $('#centralModalSm').modal('show');

    $('#file_1').append('<img src="data:image/jpg;base64,' + response28.content + '" width="250px" height="100px" alt="image">');
     break;
   case "application/pdf":
   console.log(response28.content_type);
   $("#file_2").empty();
     $("#file_1").empty();
  
     $('#centralModalSm').modal('show');
  
      $('#file_1').append('<iframe src="data:application/pdf;base64,' + response28.content + '" width=550px" height="400px"   alt="file">');
    
     break;
   // default:
   //   $('#reg_imgs').append('<img src="data:image/jpg;base64,' + image_data.content + '" width="750px" height="450px" class="img-fluid b-4" alt="Responsive image">');
 }
 
  
   //  }
 }
 

function onLogin() {
  DrawSideNav();
  var dtable = $('#datatable').DataTable({

    "ajax": {
      beforeSend: beforeSends,
      contentType: 'application/json; charset=utf-8',
      type: 'GET',
      url: INSTANCE_URL + '/api/v2/pg/_table/dp.modification_final', 
      dataSrc: function (json) {
        dtable_data = json.resource;
        // console.log(dtable_data);
        return dtable_data;
      }
    },

    "columns": dtable_columns,

    initComplete: function () {

      // Setup - add a text input to each footer cell
      $('#datatable thead tr').clone(true).appendTo('#datatable thead');
      $('#datatable thead tr:eq(1) th').each(function (i) {
        var title = $(this).text();
        $(this).html('<input type="text" placeholder="Search ' + title + '" />');

        $('input', this).on('keyup change', function () {
          if (dtable.column(i).search() !== this.value) {
            dtable.column(i).search(this.value).draw();
          }
        });

      });
      dtable.destroy();// a fix as data is dynamic but reinitalze is not really supported

      dtable = $('#datatable').DataTable({
        // select: {
        //   style: 'single'
        // },

        orderCellsTop: true,
        fixedHeader: true,
        "scrollY": 300,
        "scrollX": 200,

        dom: 'Bfrtip',
        buttons: [
          'copy', 'csv', 'excel', 'pdf', 'print'
        ]
      });
      dtable.column(0).visible(false);
      dtable.column(1).visible(false);
      $('#datatable tbody').on('click', 'button', function () {
            
        var data = dtable.row($(this).parents('tr')).data();  
      console.log(data[0]);
      console.log(data);

      var query = { content: true, is_base64: true, download: true, include_properties: true, content_type:true }
      $("#path1").click (function(){
        alert("path1");
        $.regalartech.apis('GET', '/api/v2/files/'+data[0], query, callback27);
      }); 
      $("#path2").click (function(){
        alert("path2");
       $.regalartech.apis('GET', '/api/v2/files/'+data[1], query, callback28);

}); 
      });
      

    }
  });
}