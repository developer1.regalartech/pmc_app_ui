// var selected_uuids = uuids;
var uuids1 = uuid();
var file_upload_counts=0;
var file1_path;
var file2_path;
function onLogin() {
  $.regalartech.apis('GET', "/api/v2/pg/_table/dp.modification_final", null, callback2);

  $('.datepicker').pickadate();
  $('#reservation_no').select2({
    ajax: {
      url: INSTANCE_URL + "/api/v2/pg/_table/dp.reservations",
      data: function (params) {
        var query1 = {
          filter: "sec_26_res LIKE '%" + params.term + "%'",
          fields: "sec_26_res,uuids"
        }
        return query1;
      },
      beforeSend: beforeSends,
      processResults: function (data) {
        var res = [];
        for (var i = 0; i < data.resource.length; i++) {
          item = {}
          item["id"] = data.resource[i].uuids;
          item["text"] = data.resource[i].sec_26_res;

          // selected_uuids = data.resource[i].uuids;

          res.push(item);

        }
        return {
          results: res
        };
      }
    }
  });

  $('#proposalnumber').select2(
    {
      ajax: {
        url: INSTANCE_URL + "/api/v2/pg/_table/dp.modification_proposal",
        data: function (params) {
          var query2 = {
            filter: "(proposalnumber LIKE '%" + params.term + "%') AND (reservation_no ='" + $('#reservation_no').select2('data')[0].id + "')",
            //filter: " (notification_num LIKE '%"+params.term+"%') AND (reservation_no ='87fbb71f-1eb4-433e-a1db-d53a3273fe16')",
            fields: "proposalnumber,reservation_no"

          }
          return query2;
        },

        beforeSend: beforeSends,
        processResults: function (data) {

          var res1 = [];
          for (var i = 0; i < data.resource.length; i++) {
            item = {}
            item["id"] = data.resource[i].reservation_no;
            item["text"] = data.resource[i].proposalnumber;



            res1.push(item);

          }
          return {
            results: res1

          };
        }
      }

    });
    
  $("#submit_btn").click(function () {
    file_upload_counts = 0;

    // $.regalartech.uploadOneFile( 'file_1', 'pmc', selected_uuids+'proposal_notification_number' , file_upoad_response);

    $.regalartech.uploadOneFile('file_1', 'pmc/sec_37_2/file_1_notification_num/', uuids1 ,null, file1_upoad_response);

    $.regalartech.uploadOneFile('file_2', 'pmc/sec_37_2/file_2_mod_map/', uuids1 ,null, file2_upoad_response);
   
  

  }); // submit_btn on click .. 

  var file1_upoad_response = function (file1_response) {
    // The response contains an array of group objects

    if (file1_response.name && file1_response.path && file1_response.type) {
      file_upload_counts = file_upload_counts + 1;
      console.log(file_upload_counts);
      file1_path = file1_response.path;

      if (file_upload_counts == 2) {
        postData();

      }
      
      
    }
    // console.log(sec_37);
  }
  var file2_upoad_response = function (file2_response) {
    // The response contains an array of group objects

    if (file2_response.name && file2_response.path && file2_response.type) {
      file_upload_counts = file_upload_counts + 1;
      console.log(file_upload_counts);
      file2_path = file2_response.path;

      if (file_upload_counts == 2) {
        postData();

      }
      
     
    }
    // console.log(sec_37);
  }

}// onlogin ends  here

function postData()
{
  


    var reservation_no = $('#reservation_no').select2('data');

    var proposalnumber = $('#proposalnumber').select2('data');

    post_body = {
      "resource": [
        {
          "uuids": uuids1,
          "final_not_num": $("#finalnumber").val(),
          "final_date": $('#final_noti_date').val(),
           "file_1_notification_num":file1_path,
           "file_2_mod_map":file2_path,
          // "final_short_desc": $("#short_des").val(),
          "change_area": $("#changearea").val(),
          "sec_37": $("#sec_37").val(),
          //"date": $("#prop_noti_date").val(),
          "prop_date": $("#final_noti_date").val(),
         "prop_not_num": $("#proposalnumber").val(),
          "notification_num": $("#finalnumber").val(),
          "short_desc": $("#short_des").val(),
          //"sec_37" :$("sec_37 option:selected").text(),
          "reservation_no": reservation_no[0].id,
          "proposalnumber": proposalnumber[0].id

          //  "sec_37" :sec_37[0].text

        }]
    };

    $.regalartech.apis('POST', "/api/v2/pg/_table/dp.modification_final", post_body, callback1);
    $.regalartech.apis('GET', "/api/v2/pg/_table/dp.modification_final", null, callback2);



}


$('#proposalnumber').change(function () {
  var query3 = {
    filter: "(proposalnumber LIKE '%" + $('#proposalnumber').select2('data')[0].text + "%') AND (reservation_no ='" + $('#reservation_no').select2('data')[0].id + "')",
    //filter: " (notification_num LIKE '%"+params.term+"%') AND (reservation_no ='87fbb71f-1eb4-433e-a1db-d53a3273fe16')",
    fields: "sec_37 ,date"
  };
  $.regalartech.apis('GET', "/api/v2/pg/_table/dp.modification_proposal", query3, callback23);



});
var callback23 = function (ress) {
  console.log(ress);

  $('#sec_37').val(ress[0].sec_37);
  $('#prop_noti_date').val(ress[0].date);


}
var callback1 = function (response) {
  console.log(response);
  alerts("success", "Data posted ..");
  alert("Data submited successfully ");
}
var callback2 = function (response1) {
  console.log(response1);

}
