<!DOCTYPE html>
<html lang="en">

<head>
    <?php  
require_once ($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/config.php');
require_once ($GLOBALS['app_root'].'/core_templates/headers.php');
?>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
    <style>
        @media screen and (min-width: 768px) {
            #fullHeightModalRight {
                top: 66px;
                left: auto;
                height: auto;
                bottom: auto;
                overflow: visible;
            }

            .modal-body {
                max-height: calc(100vh - 200px);
                overflow-y: auto;
            }
        }

        /* div.dataTables_wrapper
     {
        width:800px;
        margin: 25px 50px 25px 25px;
    } */
    </style>
</head>

<body onload="initialize()" class="hidden-sn black-skin">
  
    <!--/.Double navigation-->
    <!--Main Layout-->
    <main>
        <div>
            <ul class="nav nav-tabs nav-justified md-tabs indigo h6" id="myTabJust" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab-just" data-toggle="tab" href="#view_roles" role="tab"
                        aria-controls="home-just" aria-selected="true">   View Roles </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab-just" data-toggle="tab" href="#add_role" role="tab"
                        aria-controls="profile-just" aria-selected="false">Add Role </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab-just" data-toggle="tab" href="#edit_role" role="tab"
                        aria-controls="contact-just" aria-selected="false"> Edit Role  </a>
                </li>
            </ul>
            <div class="tab-content card pt-5" id="myTabContentJust">
                <div class="tab-pane fade show active" id="view_roles" role="tabpanel" aria-labelledby="home-tab-just">
                    <p>Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua,
                        retro
                        synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit
                        butcher
                        retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex
                        squid. Aliquip
                        placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate
                        nisi qui.</p>
                </div>
                <div class="tab-pane fade" id="add_role" role="tabpanel" aria-labelledby="profile-tab-just">


                    <div>

                        <div class="row py-3 h6">
                            <div class="col">
                                Role Name 
                             
                            </div>
                            <div class="col">
                            <input type="text" id="role_display_name" class="form-control" >
                            </div>
                        </div>



                        <div class="row py-3">
                            <div class="col">
                                <p class="h6">Select Organization</p>
                            </div>
                            <div class="col">
                                <select id="role_select_org"  style="width: 100%"  ></select>
                            </div>

                        </div>


                        <div class="row py-3">
                            <div class="col">
                                <p class="h6">Heap Geometry Access</p>
                            </div>
                            <div class="col">
                                <select id="role_heap_geom" class="browser-default custom-select">
                                <option value="true">Yes</option>
                                    <option selected value="false">No</option>
                                </select>
                            </div>

                        </div>
                        <div class="row py-3">
                            <div class="col">
                                <p class="h6">Heap Attribute Access</p>
                            </div>
                            <div class="col">
                                <select id="role_heap_att" class="browser-default custom-select">
                                <option value="true">Yes</option>
                                    <option selected value="false">No</option>
                                </select>
                            </div>
                        </div>



                        <div class="row py-3">
                            <div class="col">
                                <p class="h6">Heap Approval Access</p>
                            </div>
                            <div class="col">
                                <select id="role_heap_approval" class="browser-default custom-select">
                                   
                                    <option value="true">Yes</option>
                                    <option selected value="false">No</option>
                                  
                                </select>
                            </div>

                        </div>



                        <div class="row py-3">
                            <div class="col">
                                <button id="add_role_btn"type="button" class="btn btn-default btn-lg btn-block">Add Role</button>
                            </div>
                        </div>


                    </div>



                </div>
                <div class="tab-pane fade" id="edit_role" role="tabpanel" aria-labelledby="contact-tab-just">
                    <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo
                        retro
                        fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer,
                        iphone
                        skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony.
                        Leggings
                        gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever gluten-free, carles pitchfork
                        biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you probably haven't heard of them,
                        vinyl
                        craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p>
                </div>
            </div>
        </div>



    </main>
    <!--Main Layout-->



    <!--Modal: Login / Register Form-->
    <?php  require_once ($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/core_templates/login_modal.php'); ?>
    <!--Modal: Login / Register Form-->
    <!-- SCRIPTS -->
    <?php  require_once ( $GLOBALS['app_root'].'/core_templates/scripts.php'); ?>
    <!-- SCRIPTS -->
 

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>


    <script type="text/javascript" src="add_role.js"></script>
     <!--Double navigation-->
  <header>
        <!-- Sidebar navigation -->
        <?php  require_once($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/core_templates/side_nav_bar.php'); ?>
        <!--/. Sidebar navigation -->
        <!-- Navbar -->
        <?php  require_once($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/core_templates/top_nav_bar.php');    ?>
        <!-- /.Navbar -->

    </header>
</body>

</html>