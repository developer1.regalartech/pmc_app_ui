<!DOCTYPE html>
<html lang="en">

<head>

    <?php  require_once($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/core_templates/headers.php'); ?>
    <!-- Your custom styles 
    <link href="../../css/style.css" rel="stylesheet">
    <link href="../dpcell/style.css" rel="stylesheet">

    (optional) -->

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />

    <style>
    @media screen and (min-width: 768px) {
        #fullHeightModalRight {
            top: 66px;
            left: auto;
            height: auto;
            bottom: auto;
            overflow: visible;
        }

        .modal-body {
            max-height: calc(100vh - 200px);
            overflow-y: auto;
        }
    }
    </style>
</head>

<body onload="initialize()" class="hidden-sn black-skin">

    <!--Double navigation-->
    <header>
        <!-- Sidebar navigation -->
        <?php  require_once($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/core_templates/side_nav_bar.php'); ?>
        <!--/. Sidebar navigation -->


        <!-- Navbar -->
        <?php  require_once($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/core_templates/top_nav_bar.php'); ?>
        <!-- /.Navbar -->


    </header>
    <!--/.Double navigation-->

    <!--Main Layout-->
    <main>
        <div class="container-fluid">


            <div class="card">
                <div class="card-header"> Add an Organization </div>
                <div class="card-body">

                    <form class="needs-validation" novalidate>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <div>
                                    <label for="org_drop_down">Select Organization</label>
                                    <select id="org_drop_down" class="js-example-data-array"
                                        style="width: 100%"></select>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom01">Organization Id </label>
                                <input id="org_id" type="text" class="form-control" id="validationCustom01"
                                    placeholder="Enter organization name here" value="" disabled>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom01">Organization Name </label>
                                <input id="org_dis_name" type="text" class="form-control" id="validationCustom01"
                                    placeholder="Enter organization name here" value="" required>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                            </div>
                        </div>




                        <button id="update_org" class="btn btn-primary" type="submit">Update Organization</button>
                        <button id="delete_org" class="btn btn-danger" type="button">Delete Organization</button>
                    </form>


                </div>
            </div>




        </div>



    </main>
    <!--Main Layout-->

    <!--Modal: Login / Register Form-->
    <?php  require_once($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/core_templates/login_modal.php'); ?>
    <!--Modal: Login / Register Form-->


    <!-- /Start your project here-->

    <!-- SCRIPTS -->
    <?php  require_once($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/core_templates/scripts.php'); ?>
    <!-- SCRIPTS -->

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js">
    </script>
    <script type="text/javascript" charset="utf8"
        src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8"
        src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" charset="utf8"
        src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js">
    </script>
    <script type="text/javascript" charset="utf8"
        src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" charset="utf8"
        src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" charset="utf8"
        src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" charset="utf8"
        src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>


    <script type="text/javascript" src="edit_org.js"></script>




</body>

</html>