function onLogin() {
  // TO DO
  // get the org id  of the user

  $.regalartech.apis('GET', '/api/v2/pg/_table/aus.role_manager', null, null, add_org_response);


  $('#role_select_org').select2({
    ajax: {
      url: INSTANCE_URL + "/api/v2/pg/_table/aus.organization",
      data: function (params) {
        var query = {
          filter: "dis_name LIKE '%" + params.term + "%'",
          fields: "dis_name,org_id"
        }
        return query;
      },
      beforeSend: beforeSends,
      processResults: function (data) {
        var res = [];
        for (var i = 0; i < data.resource.length; i++) {
          item = {}
          item["id"] = data.resource[i].org_id;
          item["text"] = data.resource[i].dis_name;

          selected_uuids = data.resource[i].org_id;

          res.push(item);

        }
        return {
          results: res
        };
      }
    }
  });



  $("#add_role_btn").click(function () {

    role_det = {
      "resource": [{
        role_id: generateUuid(),
        dis_name: $('#role_display_name').val(),
        org_id: $('#role_select_org').select2('data')[0].id,
        role_data: '{"heap_geometry_access":' + $("#role_heap_geom option:selected").val() + ', "heap_atttribute_access": ' + $("#role_heap_att option:selected").val() + ', "heap_approval_access": ' + $("#role_heap_approval option:selected").val() + '}'
      }]
    };


    //console.log( "user role  ",   $('#register_select_role').select2('data')[0]  );
    //console.log( "user org  ",   $('#register_select_org').select2('data')[0]  );

    console.log("role_detalis   ", role_det);

    $.regalartech.apis('POST', '/api/v2/pg/_table/aus.role_manager', role_det, null, role_details_result);



  });

}

var role_details_result = function (response) {

  if (response) {
    console.log(response);
    if (response.length == 1) {
      alerts("success", "Role added succesfully");
    }
    else {
      alerts("error", "Error");
    }
  }
  else {
    alerts("error", "Error adding role");
  }






};



var add_org_response = function (session_response) {

  console.log("role table ", session_response);
  console.log("role table ", session_response[0].role_data);

  var obj = JSON.parse(session_response[0].role_data);

  console.log(obj);


};

(function () {
  'use strict';
  window.addEventListener('load', function () {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        event.preventDefault();



        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();

        } else {

          $.regalartech.apis('POST', '/api/v2/pg/_table/aus.organization', {
            "resource": [{
              dis_name: $('#org_name').val()
            }]
          }, null, add_org_response);
          console.log("Form VAILDDDDD ");

        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();