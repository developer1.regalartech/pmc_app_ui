function onLogin() {


  for (var i = 0; i < sessionStorage.length; i++) {
    // console.log(      sessionStorage.key(i) , sessionStorage.getItem(sessionStorage.key(i))         );
  }

  post_data = {
    "resource": [{
      "user_id": 0,
      "name": "test3",
      "value": "test3"
    }]
  };

  //$.regalartech.apis('GET', '/api/v2/pg/_schema/aus.users' , null, null, get_custom);
  // $.regalartech.apis('GET', '/api/v2/system/user' , null, null, get_custom);

  loadDataTable('user_list');

}

var get_custom = function (response) {
  console.log(response);


};



function loadDataTable(div_name) {

  dtable = $('#' + div_name).DataTable({
    "ajax": {
      beforeSend: beforeSends,
      contentType: 'application/json; charset=utf-8',
      type: 'GET',
      url: INSTANCE_URL + '/api/v2/pg/_table/aus.users',
      dataSrc: "resource"
    },
    
    "columns": col_def,

    initComplete: function () {

      $('#' + div_name + ' thead tr').clone(true).appendTo('#' + div_name + ' thead');
      $('#' + div_name + ' thead tr:eq(1) th').each(function (i) {
        var title = $(this).text();
        $(this).html('<input type="text" placeholder="Search ' + title + '" />');

        $('input', this).on('keyup change', function () {
          if (dtable.column(i).search() !== this.value) {
            dtable.column(i).search(this.value).draw();
          }
        });
      });
      dtable.destroy(); // a fix as data is dynamic but reinitalze is not really supported

      dtable = $('#' + div_name).DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        "scrollY": 500,
        "scrollX": 200,
        dom: 'Bfrtip',
        buttons: ['csv', 'excel']

      });
    }
  });





} // function loadDataTable ends here 

var col_def = [{
  data: "id",
  "title": "Id"
},
{
  data: "org_id",
  "title": "org id",
  "defaultContent": "<i>Not set</i>"
},
{
  data: "org_name",
  "title": "org name",
  "defaultContent": "<i>Not set</i>"
},
{
  data: "name",
  "title": "Name",
  "defaultContent": "<i>Not set</i>"
},
{
  data: "first_name",
  "title": "first name",
  "defaultContent": "<i>Not set</i>"
},
{
  data: "last_name",
  "title": "last name",
  "defaultContent": "<i>Not set</i>"
},
{
  data: "email",
  "title": "email",
  "defaultContent": "<i>Not set</i>"
},
{
  data: "last_login_date",
  "title": "last login date",
  "defaultContent": "<i>Not set</i>"
}
];