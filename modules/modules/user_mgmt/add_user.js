function onLogin() {


    for (var i = 0; i < sessionStorage.length; i++) {
       console.log(  sessionStorage.key(i) , sessionStorage.getItem(sessionStorage.key(i))         );
    }
    
  }
  
  
  $('#register_select_org').select2({
    ajax: {
      url: INSTANCE_URL + "/api/v2/pg/_table/aus.organization",
      data: function (params) {
        var query = {
          filter: "dis_name LIKE '%" + params.term + "%'",
          fields: "dis_name,org_id"
        }
        return query;
      },
      beforeSend: beforeSends,
      processResults: function (data) {
        var res = [];
        for (var i = 0; i < data.resource.length; i++) {
          item = {}
          item["id"] = data.resource[i].org_id;
          item["text"] = data.resource[i].dis_name;
  
          selected_uuids = data.resource[i].org_id;
  
          res.push(item);
  
        }
        return {
          results: res
        };
      }
    }
  });

  $('#register_select_role').select2({
    ajax: {
      url: INSTANCE_URL + "/api/v2/pg/_table/aus.role_manager",
      data: function (params) {
        var query = {
          filter: "dis_name LIKE '%" + params.term + "%'",
          fields: "dis_name,role_id"
        }
        return query;
      },
      beforeSend: beforeSends,
      processResults: function (data) {
        var res = [];
        for (var i = 0; i < data.resource.length; i++) {
          item = {}
          item["id"] = data.resource[i].role_id;
          item["text"] = data.resource[i].dis_name;
  
          selected_uuids = data.resource[i].role_id;
  
          res.push(item);
  
        }
        return {
          results: res
        };
      }
    }
  });
  
  
  (function () {
    'use strict';
    window.addEventListener('load', function () {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function (form) {
        form.addEventListener('submit', function (event) {
          event.preventDefault();
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          } else 

          {
            // var register_data = {
            //   "email": $('#register_email').val() ,
            //   "first_name": $('#register_fname').val(),
            //   "last_name": $('#register_lname').val(),
            //   "display_name": $('#register_dname').val(),
            //   "new_password": $('#register_pass').val()
             
            // };

            var register_data = {
              "resource": [
                {
                  "name":  $('#register_dname').val(),
                  "display_name": $('#register_dname').val(),
                  "email":  $('#register_email').val() ,
                  "is_active": true,
                  "first_name": $('#register_fname').val(),
                  "last_name": $('#register_lname').val(),                
                  "password": $('#register_pass').val()
                }
              ]
            };
            console.log("Form VAILDDDDD " , register_data);
            $.regalartech.apis('POST', '/api/v2/system/user?send_invite=false&session_token=abc.123.efg' , register_data , null, register_result);
          
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();
  

  
  var register_result = function (response) {
   
    console.log(response);

    if (response) {
    
      alerts("success", "User added succesfully");
      $.regalartech.apis('GET', '/api/v2/pg/_table/user', {  filter:"email='"+$('#register_email').val()+"'"  }, null, get_details);

    }else{
      alerts("error", "Failed to add user !");
    }
    

  };


  var get_details = function (response) {

   user_det =  {"resource":[ {
                              id : response[0].id,
                              role_id : $('#register_select_role').select2('data')[0].id  ,
                              org_id : $('#register_select_org').select2('data')[0].id
                            }
                        ]};

   $.regalartech.apis('POST', '/api/v2/pg/_table/aus.user_details' , user_det , null, user_details_result);
   

  };

  var user_details_result = function (response) {

    console.log(response);
    if (response && response.length ==1 )
    {
      alerts("success", "Organization assigned successfully");
      
    }
    else{
      alerts("error", "Failed to assign organization");
    }
   
  };

