<!DOCTYPE html>
<html lang="en">
<head>
    <?php  
    require_once ($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/config.php');
    require_once ($GLOBALS['app_root'].'/core_templates/headers.php');
    ?>


    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />

    <style>
    @media screen and (min-width: 768px) {
        #fullHeightModalRight {
            top: 66px;
            left: auto;
            height: auto;
            bottom: auto;
            overflow: visible;
        }

        .modal-body {
            max-height: calc(100vh - 200px);
            overflow-y: auto;
        }
    }
    </style>
</head>

<body onload="initialize()" class="hidden-sn black-skin">

    <!--Double navigation-->
    <header>
         <!-- Sidebar navigation -->
         <?php  require_once ( $GLOBALS['app_root'].'/core_templates/side_nav_bar.php'); ?>
        <!--/. Sidebar navigation -->
        <!-- Navbar -->
        <?php  require_once ( $GLOBALS['app_root'].'/core_templates/top_nav_bar.php'); ?>
        <!-- /.Navbar -->
        <!--Modal: Login / Register Form-->
        <?php  require_once ($GLOBALS['app_root'].'/core_templates/login_modal.php'); ?>
        <!--Modal: Login / Register Form-->


    </header>
    <!--/.Double navigation-->

    <!--Main Layout-->
    <main>
        <div class="container-fluid">


            <div class="card">
                <div class="card-header"> Add an User </div>
                <div class="card-body">

                    <form class="needs-validation" novalidate>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <div>
                                    <label for="register_select_org">Select Organization</label>
                                    <select id="register_select_org" class="form-control"></select>
                                </div>
                            </div>
                        </div>
             
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <div>
                                    <label for="register_select_role">Select Role </label>
                                    <select id="register_select_role" class="form-control"></select>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom01">Email  </label>
                                <input id="register_email" type="text" class="form-control" id="validationCustom01" placeholder="Enter user email here"
                                    value="testmail1@aus.co.in" required>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom02">First Name  </label>
                                <input id="register_fname" type="text" class="form-control" id="validationCustom02" placeholder="Enter user first  name here"
                                    value="fname test1" required>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom03">Last Name  </label>
                                <input id="register_lname" type="text" class="form-control" id="validationCustom03" placeholder="Enter last  name here"
                                    value="lname test" required>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom04">Display Name  </label>
                                <input id="register_dname" type="text" class="form-control" id="validationCustom04" placeholder="Enter display name here"
                                    value="Test display name" required>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom05"> Password</label>
                                <input id="register_pass" type="password" class="form-control" id="validationCustom05" placeholder="Enter password name here"
                                    value="testpass@567" required>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                            </div>
                        </div>


                        <button id="update_org" class="btn btn-primary" type="submit"> Register User </button>
                        
                    </form>


                </div>
            </div>




        </div>



    </main>
    <!--Main Layout-->




    <!-- /Start your project here-->

    <!-- SCRIPTS -->
    <?php  require_once ( $GLOBALS['app_root'].'/core_templates/scripts.php'); ?>
    <!-- SCRIPTS -->


    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>


    <script type="text/javascript" src="add_user.js"></script>




</body>

</html>