function onLogin() {


  for (var i = 0; i < sessionStorage.length; i++) {
     console.log(  sessionStorage.key(i) , sessionStorage.getItem(sessionStorage.key(i))         );
  }
  
}


$('#org_drop_down').select2({
  ajax: {
    url: INSTANCE_URL + "/api/v2/pg/_table/aus.organization",
    data: function (params) {
      var query = {
        filter: "dis_name LIKE '%" + params.term + "%'",
        fields: "dis_name,org_id,test1"
      }
      return query;
    },
    beforeSend: beforeSends,
    processResults: function (data) {
      var res = [];
      for (var i = 0; i < data.resource.length; i++) {
        item = {}
        item["id"] = data.resource[i].org_id;
        item["text"] = data.resource[i].dis_name;

        selected_uuids = data.resource[i].org_id;

        res.push(item);

      }
      return {
        results: res
      };
    }
  }
});

$('#org_drop_down').on("select2:select", function (e) {


  filter = {
    filter: 'org_id=' + e.params.data.id
  };
  $.regalartech.apis('GET', '/api/v2/pg/_table/aus.organization', filter, null, fill_form);

});


var fill_form = function (response) {

  $('#org_id').val(response[0].org_id);
  $('#org_dis_name').val(response[0].dis_name);



};


(function () {
  'use strict';
  window.addEventListener('load', function () {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        event.preventDefault();
      
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        } else 
        {
          //$.regalartech.apis('POST', '/api/v2/pg/_table/aus.organization', {"resource":[   {dis_name: $('#org_dis_name').val()}  ]}    , null, add_org_response );
          $.regalartech.apis('PATCH', '/api/v2/pg/_table/aus.organization/' + $('#org_id').val(), {dis_name: $('#org_dis_name').val()}, null, patch_result);
          // console.log("Form VAILDDDDD ");
        }

        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

$("#delete_org").click(function(){

 
  $.regalartech.apis('DELETE', '/api/v2/pg/_table/aus.organization/' + $('#org_id').val(), null, null, delete_result);

});

var patch_result = function (response) {
  $("#org_drop_down").val('').trigger('change');
  console.log(response);
};
var delete_result = function (response) {
  $("#org_drop_down").val('').trigger('change');
  console.log(response);
};