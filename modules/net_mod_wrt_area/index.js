var window_title = 'RegalarTech Pvt Ltd'
var navbar_title = 'PMC - Development Plan Cell'

function initialize() {
  checkSession();
  initalizeSideNav();

}
// --------------------------------------  UI  ----------------------------------------------------

function initalizeSideNav()
{
  document.title = window_title;
  $('project#project-title').html("&nbsp; &nbsp; " + navbar_title);

  $(".button-collapse").sideNav();
  var sideNavScrollbar = document.querySelector('.custom-scrollbar');
  Ps.initialize(sideNavScrollbar);



}
