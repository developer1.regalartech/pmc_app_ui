








<!DOCTYPE html>
<html lang="en">
<head>




  
      <?php  

      require_once ($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/config.php');
      require_once ($GLOBALS['app_root'].'/core_templates/headers.php');

      ?>
 
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <link href="../../core/css/addons/datatables.min.css" rel="stylesheet">
  <link href="style.css" rel="stylesheet">
  <link href="alert.css" rel="stylesheet">
  
  <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" rel="stylesheet"/> -->
  <style>
    @media screen and (min-width: 768px) {
      #toast-container {
        z-index: 99;
      }
      #fullHeightModalRight {
        top: 66px;
        left: auto;
        height: auto;
        bottom: auto;
        overflow: visible;
      }
      .modal-body {
        max-height: calc(100vh - 200px);
        overflow-y: auto;
      }
    }
    table.dataTable thead th,
    table.dataTable thead td {
      padding: 7px 105px;
      border-bottom: 1px solid #243A51;
    }
 </style>
</head>

<body onload="initialize()" class="hidden-sn mdb-skin">
  <!--Double navigation-->
  <header>
  <?php  
        require_once ($GLOBALS['app_root'].'/core_templates/side_nav_bar.php'); ?>
        <!--/. Sidebar navigation -->
        <!-- Navbar -->
 <?php   require_once ($GLOBALS['app_root'].'/core_templates/top_nav_bar.php'); ?>

    
      
  </header>
  
        <?php  

        require_once ($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/config.php');
        require_once ($GLOBALS['app_root'].'/core_templates/headers.php');

        ?>
  <main>
        <div class="container">
                <div class="row">
                  <div class="col-md-2"></div>
                  <div class="col-md-8">
                    <div class="card textfont">
                      <div class="card-header fontalign">
                         Section 49(1) Notice To State Govt
                      </div>
                      <div class="card-body">
                        <!-- <h5 class="card-title">Special title treatment</h5> -->
                        <form class="text-center border border-light p-5">
                          <div class="card">
                            <div class="card-body">
                              <!-- <h4>Proposal</h4> -->

                              <div>
                                  <label for="reservation_no">Reservation Number</label>
                                <select id="reservation_no" class="js-example-data-array" style="width: 100%"></select>
                              </div>
                            
                              <div>
                                <label for="InWord_Number">Inward Number</label>
                                <input type="text" id="InWord_Number" class="form-control mb-4" placeholder="Inward Number">
                              </div>
                          
                              <div class="md-form">
                                <label for="date"> Date</label>
                                <input placeholder="Date" type="text" id="date" class="form-control datepicker">
                              </div>
                              
                           
                            </div>
                          </div>
                          <br>            
                        
                          <div class="form-group" id="id1">
                           <div class="main-alert body1" id="id2">  
                               <i class="icon bg-success text-white float-left"></i>
                               <div class="alert">
                                   <span>successfull data inserted</span>
                                   <i class="text-success"></i>
                               </div>
                           </div>
                            <!-- <button type="button" class="btn btn-danger" id="btn_reject">Reject</button> -->
                              <button type="button" class="btn btn-success" id="submit_btn"> Save </button>
                            
                          </div>
          
                        </form>
                        <!-- </div> -->
                      </div>
                      </div>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>

  </main>
  <!--Main Layout-->
  <?php  require_once($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/core_templates/login_modal.php'); ?>

    <!--Modal: Login / Register Form-->
  <!-- /Start your project here-->
  <!-- SCRIPTS -->
  <!-- JQuery -->
  <?php   require_once ($GLOBALS['app_root'].'/core_templates/scripts.php'); ?>
 
  <script type="text/javascript" src="section49_1.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css"></script> -->
  <script type="text/javascript" src="section49_1.js"></script>
  <!-- <script type="text/javascript" src="sliderbars.js"></script> -->
</body>
</html>