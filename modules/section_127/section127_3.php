<!DOCTYPE html>
<html lang="en">

<head>

<?php  

require_once ($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/config.php');
require_once ($GLOBALS['app_root'].'/core_templates/headers.php');

?>


    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link href="../../core/css/addons/datatables.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
    <style>
        @media screen and (min-width: 768px) {
            #toast-container {
                z-index: 99;
            }
            #fullHeightModalRight {
                top: 66px;
                left: auto;
                height: auto;
                bottom: auto;
                overflow: visible;
            }
            .modal-body {
                max-height: calc(100vh - 200px);
                overflow-y: auto;
            }
        }

        table.dataTable thead th,
        table.dataTable thead td {
            padding: 7px 105px;
            border-bottom: 1px solid #243A51;
        }
    </style>
</head>

<body onload="initialize()" class="hidden-sn mdb-skin">
    <!--Double navigation-->
    <header>
    <?php  
            require_once ($GLOBALS['app_root'].'/core_templates/side_nav_bar.php'); ?>
            <!--/. Sidebar navigation -->
            <!-- Navbar -->
    <?php   require_once ($GLOBALS['app_root'].'/core_templates/top_nav_bar.php'); ?>

    </header>
    <!--/.Double navigation-->
    <!--Main Layout-->
    <main>
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="card textfont">
                        <div class="card-header fontalign">
                            Section 127 Notice By Own
                        </div>
                        <div class="card-body">
                            <!-- <h5 class="card-title">Special title treatment</h5> -->
                            <form class="text-center border border-light p-5">
                                <div class="card">
                                    <div class="card-body">
                                        <!-- <h4>Proposal</h4> -->
                                        <div>  
                                            <label for="reservation_no">Reservation Number</label>
                                            <select id="reservation_no" class="js-example-data-array" style="width: 100%"></select>
                                        </div>
                                        
                                        <div>
                                            <label for="date1">Dp Enforcement Date + 10 year  </label>
                                            <input type="text" id="date1" class="form-control mb-4" placeholder="Dp Enforcement Date"  disabled>
                                        </div>
                                    
                                        <div class="input-default-wrapper mt-3">
                                            <span class="input-group-text mb-3" id="input1">Inword Number</span>
                                            <input type="file" id="file_1" class="input-default-js">
                                            <label class="label-for-default-js rounded-right mb-3" for="file_1">
                                                <span class="span-choose-file">Choose file
                                                </span>
                                                <div class="float-right span-browse">Browse</div>
                                            </label>
                                        </div>
                                      
                                        <div>
                                            <label for="date"> Date</label>
                                            <input placeholder="Date" type="text" id="date" class="form-control datepicker">
                                        </div>
                                        <div>
                                            <label for="notice">Notice</label>
                                            <input type="text" id="notice" class="form-control mb-4" placeholder="Notice">
                                        </div>



                                    </div>
                                </div>


                                <div class="form-group">
                                    <!-- <button type="button" class="btn btn-danger" id="btn_reject">Reject</button> -->
                                    <button type="button" class="btn btn-success" id="submit_btn"> Save </button>

                                </div>

                            </form>
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>

    </main>
    <!--Main Layout-->
    <!--Modal: Login / Register Form-->
    <?php  require_once($_SERVER['DOCUMENT_ROOT'].'/pmc_app_ui/core_templates/login_modal.php'); ?>
    <!--Modal: Login / Register Form-->
    <!-- /Start your project here-->
    <!-- SCRIPTS -->
    <!-- JQuery -->
    <?php   require_once ($GLOBALS['app_root'].'/core_templates/scripts.php'); ?>
  
    <script type="text/javascript" src="section127_3.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript" src="index.js"></script>
    <!-- <script type="text/javascript" src="sliderbars.js"></script> -->
</body>

</html>