function onLogin() {


  
    loadDataTable('reservation_list');
  
  }
  
  var get_custom = function (response) {
    console.log(response);
  
  
  };
  
  
  
  function loadDataTable(div_name) {
  
    dtable = $('#' + div_name).DataTable({
      "ajax": {
        beforeSend: beforeSends,
        contentType: 'application/json; charset=utf-8',
        type: 'GET',
        url: INSTANCE_URL + '/api/v2/pg/_table/dp.reservations',
        dataSrc: "resource"
      },
      
      "columns": col_def,
  
      initComplete: function () {
  
        $('#' + div_name + ' thead tr').clone(true).appendTo('#' + div_name + ' thead');
        $('#' + div_name + ' thead tr:eq(1) th').each(function (i) {
          var title = $(this).text();
          $(this).html('<input type="text" placeholder="Search ' + title + '" />');
  
          $('input', this).on('keyup change', function () {
            if (dtable.column(i).search() !== this.value) {
              dtable.column(i).search(this.value).draw();
            }
          });
        });
        dtable.destroy(); // a fix as data is dynamic but reinitalze is not really supported
  
        dtable = $('#' + div_name).DataTable({
          orderCellsTop: true,
          fixedHeader: true,
          "scrollY": 500,
          "scrollX": 200,
          dom: 'Bfrtip',
          buttons: ['csv', 'excel']
  
        });
      }
    });
  
  
  
  } // function loadDataTable ends here 
  
  var col_def = [{
    data: "zone",
    "title": "Zone",
    "defaultContent": "<i>Not set</i>"
  },
  {
    data: "sector",
    "title": "Sector",
    "defaultContent": "<i>Not set</i>"
  },
  {
    data: "sec_26_res",
    "title": "sec_26_res",
    "defaultContent": "<i>Not set</i>"
  },
  {
    data: "sec_26_area",
    "title": "sec_26_area",
    "defaultContent": "<i>Not set</i>"
  },{
    data: "sec_26_cts_fp_sur",
    "title": "sec_26_cts_fp_sur",
    "defaultContent": "<i>Not set</i>"
  },
  {
    data: "sec_26_village",
    "title": "sec_26_village",
    "defaultContent": "<i>Not set</i>"
  },
  {
    data: "sec_28_mod_no",
    "title": "sec_28_mod_no",
    "defaultContent": "<i>Not set</i>"
  },{
    data: "sec_31_sm_ep",
    "title": "sec_31_sm_ep",
    "defaultContent": "<i>Not set</i>"
  }
  ,
  {
    data: "sec_31_final",
    "title": "sec_31_final",
    "defaultContent": "<i>Not set</i>"
  },
  {
    data: "sec_31_sanctioned",
    "title": "sec_31_sanctioned",
    "defaultContent": "<i>Not set</i>"
  },{
    data: "col11",
    "title": "col11",
    "defaultContent": "<i>Not set</i>"
  }
  ,
  {
    data: "col12",
    "title": "col12",
    "defaultContent": "<i>Not set</i>"
  },
  {
    data: "col13",
    "title": "col13",
    "defaultContent": "<i>Not set</i>"
  },{
    data: "development_status",
    "title": "development_status",
    "defaultContent": "<i>Not set</i>"
  }
  ,
  {
    data: "remark",
    "title": "remark",
    "defaultContent": "<i>Not set</i>"
  },
  {
    data: "uuids",
    "title": "uuids",
    "defaultContent": "<i>Not set</i>"
  },{
    data: "gid",
    "title": "gid",
    "defaultContent": "<i>Not set</i>"
  }
  
  
  
  ];