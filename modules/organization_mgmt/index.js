var dtable ;
function onLogin()
{
  //sessionStorage.clear();
  for (var i = 0; i < sessionStorage.length; i++){
    console.log( sessionStorage.key(i) , sessionStorage.getItem(sessionStorage.key(i))         );
   
  }

  loadDataTable('datatable');
} // function onLogin  ends here


function loadDataTable(div_name)
{
  
  dtable = $('#'+div_name ).DataTable( {
    "ajax":
    {
      beforeSend:beforeSends,
      contentType: 'application/json; charset=utf-8',
      type : 'GET',
      url: INSTANCE_URL+'/api/v2/pg/_table/aus.organization',
      dataSrc: "resource"
    },
    "columns": [
                  { data: "org_id"  ,  "title": "Id" },
                  { data: "dis_name"  ,  "title": "Name" , "defaultContent": "<i>Not set</i>" }
              ],
  
    initComplete: function () {
  
          // Setup - add a text input to each footer cell
         
          $('#'+div_name+' thead tr').clone(true).appendTo( '#'+div_name+' thead' );
          $('#'+div_name+' thead tr:eq(1) th').each( function (i) {
              var title = $(this).text();
              $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
  
              $( 'input', this ).on( 'keyup change', function () {
                  if ( dtable.column(i).search() !== this.value ) {
                      dtable.column(i).search( this.value ).draw();
                  }
              } );
          } );
          dtable.destroy();// a fix as data is dynamic but reinitalze is not really supported
  
          dtable = $('#'+div_name).DataTable( {
              orderCellsTop: true,
              fixedHeader: true,
              "scrollY": 500,
              "scrollX": 200,
              dom: 'Bfrtip',
              buttons: ['csv', 'excel']
             
          } );
  
    
          }
  
          
  
        });
   
  



}  // function loadDataTable ends here 

