//Defining map as a global variable to access from other functions
INSTANCE_URL = 'http://gis.regalartech.com:8282';
APP_API_KEY = '552fc18d4bb13e350166e434b9106c6d312c966794cbf72c33e6eb9ef612e6bd';
IDLE_TIMEOUT = 1000 * 60 * 10; // minutes
var login_modal_id = '#regalar-login-register';
var User_role_data
var get_role_id = function (response) {

    console.log(response)

    //get data about the role which was assigned to user
    $.regalartech.apis('GET', '/api/v2/pg/_table/dp.role_manager', { filter: "role_id=" + response[0].role_id }, null, get_role_data);
}

var get_role_data = function (response) {

    console.log(response)

    User_role_data = JSON.parse(response[0].role_data)

    rearrangeSideBar()
}
var addSessionInfo = function (session_response) {
    $.each(session_response, function (key, value) {
        sessionStorage.setItem(key, value);
    });

    onLogin();
};

var loginHandle = function (response) {
    if (response.hasOwnProperty('session_token')) {
        alerts("success", "Login success");

        $.each(response, function (key, value) {
            sessionStorage.setItem(key, value);
            //console.log("login set ",key," ->", value);
        });
        sessionStorage.setItem("session_token", response.session_token);
        $(login_modal_id).modal('hide');
        $.regalartech.apis('GET', '/api/v2/user/session', null, null, addSessionInfo);


    }
    else {
        if (response) {
            alerts("error", "" + response.responseJSON.error.message);
        }
    }
};

function removeToken() {
    $.regalartech.logout(function () {
        //console.log(data);
        alerts("info", "Manual or Inactivity Logout success");
        sessionStorage.clear();
        $(login_modal_id).modal('show');
    });
}

function checkSession() {

    if (typeof (Storage) !== "undefined") {
        if (sessionStorage.getItem("session_token") === null) {
            $(login_modal_id).modal('show');
        }
        else {
            alerts("info", "Page refreshed, session continued .. ");
             $.regalartech.apis('GET', '/api/v2/pg/_table/dp.user_details', { filter: "id=" + sessionStorage.id }, null, get_role_id);

            onLogin();
        }
    } else {
        alerts("error", "No web Storage Support !");
    }
}

// login
$(document)
    .on('submit', '#l-form', function () {
        alerts("info", "Please wait for authentication");
        $.regalartech.login($('#login_username').val(), $('#login_password').val(), loginHandle);
        return false;
    })
    .idle({
        onIdle: function () {
            removeToken();
        },
        idle: IDLE_TIMEOUT
    });


// logout
$("#reg-out").click(function () { removeToken(); });

function submit_post_via_hidden_form(url, params) {
    var f = $("<form target='_blank' method='POST' style='display:none;'></form>").attr({
        action: url
    }).appendTo(document.body);

    for (var i in params) {
        if (params.hasOwnProperty(i)) {
            $('<input type="hidden" />').attr({
                name: i,
                value: params[i]
            }).appendTo(f);
        }
    }
    f.submit();
    f.remove();
}



function rearrangeSideBar() {
    var allTables = Object.keys(User_role_data)

    for (i = 0; i < allTables.length; i++) {
        var thatLi = document.getElementById(allTables[i])
        var subUL = thatLi.getElementsByTagName('ul')[0]
        subUL.innerHTML = ''
        liinul = ''
        if (User_role_data[allTables[i]] != "" && User_role_data[allTables[i]] != undefined) {
            var arrayOfassignedVal = User_role_data[allTables[i]].split(',')
            if (arrayOfassignedVal.includes("5")) {
                liinul += '<li><a href="../' + allTables[i] + '/index_' + allTables[i] + '.php"  class="waves-effect"><i class="fa fa-list"></i>  View ' + allTables[i] + '</a> </li><li><a href="../' + allTables[i] + '/add_' + allTables[i] + '.php" class="waves-effect"><i class="fa fa-plus"></i>  Add ' + allTables[i] + '</a> </li><li><a href="../' + allTables[i] + '/edit_' + allTables[i] + '.php" class="waves-effect"><i class="fa fa-pencil"></i>  Edit ' + allTables[i] + '</a> </li><li><a href="../' + allTables[i] + '/delete_' + allTables[i] + '.php" class="waves-effect"><i class="fa fa-trash"></i>  Delete ' + allTables[i] + '</a> </li>'
            } else {
                if (arrayOfassignedVal.includes("4")) {
                    liinul += '<li><a href="../' + allTables[i] + '/index_' + allTables[i] + '.php"  class="waves-effect"><i class="fa fa-list"></i>  View ' + allTables[i] + '</a> </li>'
                }
                if (arrayOfassignedVal.includes("3")) {
                    liinul += '<li><a href="../' + allTables[i] + '/delete_' + allTables[i] + '.php" class="waves-effect"><i class="fa fa-trash"></i>  Delete ' + allTables[i] + '</a> </li>'
                }
                if (arrayOfassignedVal.includes("2")) {
                    liinul += '<li><a href="../' + allTables[i] + '/edit_' + allTables[i] + '.php" class="waves-effect"><i class="fa fa-pencil"></i>  Edit ' + allTables[i] + '</a> </li>'
                }
                if (arrayOfassignedVal.includes("1")) {
                    liinul += '<li><a href="../' + allTables[i] + '/add_' + allTables[i] + '.php" class="waves-effect"><i class="fa fa-plus"></i>  Add ' + allTables[i] + '</a> </li>'
                }
            }
            subUL.innerHTML = liinul
        } else {
            thatLi.style.display = 'none'
        }
    }
}
