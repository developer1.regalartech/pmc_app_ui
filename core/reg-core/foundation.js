//Defining map as a global variable to access from other functions
var INSTANCE_URL   = ''; // define it in jwt-ui.js
var APP_API_KEY     = ''; // define it in jwt-ui.js
var IDLE_TIMEOUT = 0; // min
var window_title = 'AUS Pvt Ltd';
var navbar_title = 'Mine and Stock Management System';
var post_obj = {};
// -------------------------------------------------------
if (window.File && window.FileReader && window.FileList && window.Blob) {
    // Great success! All the File APIs are supported.
  } else {
    alert('Error:The File APIs are not fully supported in this browser. you wont able able to upload files to server.');
  }
  
  window.generateUuid=function()
{
    var seed = Date.now();
    if (window.performance && typeof window.performance.now === "function") {
        seed += performance.now();
    }

    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (seed + Math.random() * 16) % 16 | 0;
        seed = Math.floor(seed/16);

        return (c === 'x' ? r : r & (0x3|0x8)).toString(16);
    });

    return uuid;
};

beforeSends =function(request) {
    request.setRequestHeader('X-DreamFactory-Api-Key',''+APP_API_KEY);
    request.setRequestHeader('X-DreamFactory-Session-Token',''+ sessionStorage.getItem("session_token"));

};

  
function  beforeSendAddHeaders (request, headers ){
    
    request.setRequestHeader('X-DreamFactory-Api-Key',''+APP_API_KEY);
    request.setRequestHeader('X-DreamFactory-Session-Token',''+ sessionStorage.getItem("session_token"));
    if (headers)
    {
        for (var key in headers) {
            if( headers.hasOwnProperty(key) ) {
              console.log(key + " , " + headers[key] );
              request.setRequestHeader(key,headers[key]);
            } 
        }
    }
};


function setToken(key, value) {
    sessionStorage.setItem(key, value);
}
function getToken(key) {
    return sessionStorage.getItem(key);
}

function removeToken(key) {

    $.regalartech.logout(function(data) {
        if(data.success) {
            sessionStorage.removeItem(key);
          //  $.route('index');
        }
        else {
            var response = parseResponse(data);
          //  messageBox(response.code, response.message, response.error);
        }
    });
}

function clearForm() {
    $('input').each(function(){
        $(this).val('');
    });
}


(function($)
{
    $.extend({
        regalartech: {

            apis: function(rest_type, rest_url, body_params, headers, callback) {
                $.ajax({
                    dataType: 'json',
                    url: INSTANCE_URL + rest_url,
                    data: body_params,
                    cache:false,
                    method:rest_type ,
                    beforeSend: function(request) { beforeSendAddHeaders (request, headers )} ,
                    success:function (response) {
                        if(typeof callback !== 'undefined') {
                            if (response.hasOwnProperty('resource'))
                                callback(response.resource);
                            else
                                callback(response);
                        }
                    },
                    error:function (response) {
                       
                        callback(response);
                    }
                });
            },

            redirectPost: function(location, target, args){
                
                var form = '';
                $.each( args, function( key, value ) {
                    value = value.split('"').join('\"')
                    form += '<input type="hidden" name="'+key+'" value="'+value+'">';
                });
                $('<form  target= "'+target+'" action="' + location + '" method="POST">' + form + '</form>').appendTo($(document.body)).submit();
            },

            login: function(email, password, callback) {
                $.ajax({
                    dataType: 'json',
                    timeout: 5000,
                    contentType: 'application/json',
                    url: INSTANCE_URL + '/api/v2/user/session',
                    data: JSON.stringify({
                        "email": email,
                        "password": password
                    }),
                    cache:false,
                    method:'POST',
                    timeout: 3000,
                    success:function (response) {
                        callback(response);
                    },
                    error:function (response) {
                        callback(response);
                        return false;
                    }
                });
            },

            logout: function(callback) {
                $.ajax({
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    url: INSTANCE_URL + '/api/v2/user/session',
                    cache:false,
                    method:'DELETE',
                    timeout: 3000,
                    success:function (response) {
                        callback(response);
                        sessionStorage.clear();
                    },
                    error:function (response) {
                        sessionStorage.clear();
                        callback(response);
                        return false;
                    }
                });
            },

            register: function(firstname, lastname, email, password, callback) {
                $.ajax({
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    url: INSTANCE_URL + '/api/v2/user/register?login=true',
                    data: JSON.stringify({
                        "first_name": firstname,
                        "last_name": lastname,
                        "email": email,
                        "new_password": password
                    }),
                    cache:false,
                    method:'POST',
                    timeout: 3000,
                    success:function (response) {
                        callback(response);
                    },
                    error:function (response) {
                        callback(response);
                        return false;
                    }
                });
            },
            
            listSupportingScripts: function( params, callback) {
                $.ajax({
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    url: INSTANCE_URL + '/api/v2/system/script_type',
                    data: params,
                    cache:false,
                    method:'GET',
                    beforeSend: function(request) { beforeSendAddHeaders (request, headers )},
                    success:function (response) {
                        if(typeof callback !== 'undefined') {
                            if (response.hasOwnProperty('resource'))
                                callback(response.resource);
                            else
                                callback(response);
                        }
                    },
                    error:function (response) {
                        callback(response);
                        return false;
                    }
                });
            },



            getRecords: function(table, params, callback) {
                $.ajax({
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    url: INSTANCE_URL + '/api/v2/pg/_table/' + table,
                    data: params,
                    cache:false,
                    method:'GET',
                    beforeSend: function(request) { beforeSendAddHeaders (request, headers )},
                    success:function (response) {
                        if(typeof callback !== 'undefined') {
                            if (response.hasOwnProperty('resource'))
                                callback(response.resource);
                            else
                                callback(response);
                        }
                    },
                    error:function (response) {
                        callback(response);
                        return false;
                    }
                });
            },


            callFunction: function(calledFunction, params, callback) {
                $.ajax({
                    dataType: 'json',
                   
                    url: INSTANCE_URL + '/api/v2/pg/_func/' + calledFunction,
                    data: params,
                    cache:false,
                    method:'POST',
                    beforeSend: function(request) { beforeSendAddHeaders (request, headers )},
                    success:function (response) {
                        if(typeof callback !== 'undefined') {
                            if (response.hasOwnProperty('resource'))
                                callback(response.resource);
                            else
                                callback(response);
                        }
                    },
                    error:function (response) {
                        callback(response);
                        return false;
                    }
                });
            },


            callGetCustomScript: function(calledFunction, params, callback) {
                $.ajax({
                    dataType: 'json',
                   
                    url: INSTANCE_URL + calledFunction,
                    data: params,
                    cache:false,
                    method:'GET',
                    beforeSend: function(request) { beforeSendAddHeaders (request, headers )},
                    success:function (response) {
                        if(typeof callback !== 'undefined') {
                            if (response.hasOwnProperty('resource'))
                                callback(response.resource);
                            else
                                callback(response);
                        }
                    },
                    error:function (response) {
                        callback(response);
                        return false;
                    }
                });
            },

            setRecord: function(table, params, callback) {
                $.ajax({
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    url: INSTANCE_URL + '/api/v2/pg/_table/' + table,
                    data: params,
                    cache:false,
                    method:'POST',
                    beforeSend: function(request) { beforeSendAddHeaders (request, headers )},
                    success:function (response) {
                        if(typeof callback !== 'undefined') {
                            if (response.hasOwnProperty('resource'))
                                callback(response.resource);
                            else
                                callback(response);
                        }
                    },
                    error:function (response) {
                        callback(response);
                        return false;
                    }
                });
            },

            // temp ..
            updateOneRecord: function(table, rec_id, params, headers ,callback) {
                $.ajax({
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    url: INSTANCE_URL + '/api/v2/pg/_table/' + table + '/' + rec_id,
                    data: params,
                    cache:false,
                    method:'PATCH',
                    beforeSend: function(request) { beforeSendAddHeaders (request, headers )},
                    success:function (response) {
                        if(typeof callback !== 'undefined') {
                            if (response.hasOwnProperty('resource'))
                                callback(response.resource);
                            else
                                callback(response);
                        }
                    },
                    error:function (response) {
                        callback(response);
                        return false;
                    }
                });
            },


            updateRecord: function(table, params, callback) {
                $.ajax({
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    url: INSTANCE_URL + '/api/v2/pg/_table/' + table + '/',
                    data: params,
                    cache:false,
                    method:'PATCH',
                    beforeSend: function(request) { beforeSendAddHeaders (request, headers )},
                    success:function (response) {
                        if(typeof callback !== 'undefined') {
                            if (response.hasOwnProperty('resource'))
                                callback(response.resource);
                            else
                                callback(response);
                        }
                    },
                    error:function (response) {
                        callback(response);
                        return false;
                    }
                });
            },

            deleteRecord: function(table, params, callback) {
                $.ajax({
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    url: INSTANCE_URL + '/api/v2/pg/_table/' + table + '?' + params,
                    //data: params,
                    cache:false,
                    method:'DELETE',
                    beforeSend: function(request) { beforeSendAddHeaders (request, headers )},
                    success:function (response) {
                        if(typeof callback !== 'undefined') {
                            if (response.hasOwnProperty('resource'))
                                callback(response.resource);
                            else
                                callback(response);
                        }
                    },
                    error:function (response) {
                        callback(response);
                        return false;
                    }
                });
            },

            replaceRecord: function(table, params, callback) {
                $.ajax({
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    url: INSTANCE_URL + '/api/v2/pg/_table/' + table,
                    data: params,
                    cache: false,
                    method: 'PUT',
                    beforeSend: function(request) { beforeSendAddHeaders (request, headers )},
                    success: function (response) {
                        if(typeof callback !== 'undefined') {
                            if (response.hasOwnProperty('resource'))
                                callback(response.resource);
                            else
                                callback(response);
                        }
                    },
                    error: function (response) {
                        callback(response);
                        return false;
                    }
                });
            },
            uploadOneFile: function( upload_div_name , file_path , file_name,  callback ) 
            {
                var fullPath = document.getElementById(''+upload_div_name).value;
                var filename_with_ext = fullPath.substring(fullPath.lastIndexOf('\\')+1);
                if (fullPath  && file_name == null ) {
                    var filename_with_ext = fullPath.substring(fullPath.lastIndexOf('\\')+1);
                    file_name= filename_with_ext;
                }
                else // use the file name given by user but do attach the original file extension
                {file_name = file_name+'.'+fullPath.split('.').pop(); }
                var file_data = $('#'+upload_div_name).get(0).files.item(0);
            
                $.ajax({
                    url: INSTANCE_URL + '/api/v2/files/'+ file_path + file_name,
                    data: file_data,
                    cache:false,
                    contentType: false, // does not work
                    processData: false, // does not work
                    method:'POST',
                    beforeSend: function(request) { beforeSendAddHeaders (request, headers )},
                    success:function (response) {
                        if(typeof callback !== 'undefined') {
                            if (response.hasOwnProperty('resource'))
                                callback(response.resource);
                            else
                                callback(response);
                        }
                    },
                    error:function (response) {
                        callback(response);
                        return false;
                    }
                }); ///ajax ends

            } // uploadonefile ends here

            

        } // regalartech ends here 
    });

}(jQuery));

(function ($) {
  'use strict';

  $.fn.idle = function (options) {
    var defaults = {
        idle: 60000, //idle time in ms
        events: 'mousemove keydown mousedown touchstart', //events that will trigger the idle resetter
        onIdle: function () {}, //callback function to be executed after idle time
        onActive: function () {}, //callback function to be executed after back from idleness
        onHide: function () {}, //callback function to be executed when window is hidden
        onShow: function () {}, //callback function to be executed when window is visible
        keepTracking: true, //set it to false if you want to track only the first time
        startAtIdle: false,
        recurIdleCall: false
      },
      idle = options.startAtIdle || false,
      visible = !options.startAtIdle || true,
      settings = $.extend({}, defaults, options),
      lastId = null,
      resetTimeout,
      timeout;

    //event to clear all idle events
    $(this).on( "idle:stop", {}, function( event) {
      $(this).off(settings.events);
      settings.keepTracking = false;
      resetTimeout(lastId, settings);
    });

    resetTimeout = function (id, settings) {
      if (idle) {
        idle = false;
        settings.onActive.call();
      }
      clearTimeout(id);
      if(settings.keepTracking) {
        return timeout(settings);
      }
    };

    timeout = function (settings) {
      var timer = (settings.recurIdleCall ? setInterval : setTimeout), id;
      id = timer(function () {
        idle = true;
        settings.onIdle.call();
      }, settings.idle);
      return id;
    };

    return this.each(function () {
      lastId = timeout(settings);
      $(this).on(settings.events, function (e) {
        lastId = resetTimeout(lastId, settings);
      });
      if (settings.onShow || settings.onHide) {
        $(document).on("visibilitychange webkitvisibilitychange mozvisibilitychange msvisibilitychange", function () {
          if (document.hidden || document.webkitHidden || document.mozHidden || document.msHidden) {
            if (visible) {
              visible = false;
              settings.onHide.call();
            }
          } else {
            if (!visible) {
              visible = true;
              settings.onShow.call();
            }
          }
        });
      }
    });

  };
}(jQuery));
