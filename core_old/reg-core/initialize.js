function initialize() {
    checkSession();
    initalizeSideNav();
  
  }
  // --------------------------------------  UI  ----------------------------------------------------
  
  function initalizeSideNav()
  {
  
  
    $(".button-collapse").sideNav();
    var sideNavScrollbar = document.querySelector('.custom-scrollbar');
    Ps.initialize(sideNavScrollbar);
    document.title = window_title;
    console.log("setting the ui ...");
    $('#project-title').html("&nbsp; &nbsp; " + navbar_title);
  
  }
  