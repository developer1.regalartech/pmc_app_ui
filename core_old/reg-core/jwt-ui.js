//Defining map as a global variable to access from other functions
INSTANCE_URL   = 'http://gis.regalartech.com:8282';
APP_API_KEY     = '685dc71e69d6119f8057bfd799fa05dd207aa85de83ab7b2c1b1cb63e14cf119';
IDLE_TIMEOUT = 1000*60*10; // minutes
var login_modal_id = '#regalar-login-register';

var addSessionInfo = function (session_response)
{
    if(session_response.hasOwnProperty('role')) 
    {

    
    $.each( session_response, function( key, value ) {
        sessionStorage.setItem(key,value);
        //console.log("addSessionInfo set ",key," ->", value);
        
      });

      onLogin();
    }
    else
    {
        alerts("error", "Plese set role for the user !!")
    }
    
};

var loginHandle = function(response) {
    if(response.hasOwnProperty('session_token')) {
        alerts("success","Login success");

        $.each( response, function( key, value ) {
            sessionStorage.setItem(key,value);
            //console.log("login set ",key," ->", value);
          });
        sessionStorage.setItem("session_token", response.session_token);
        $(login_modal_id).modal('hide');
        $.regalartech.apis('GET', '/api/v2/user/session', null,null, addSessionInfo );
       
    }
    else {
        if(response) {
        alerts("error",""+response.responseJSON.error.message);
        }
    }
};

function removeToken()
{
    $.regalartech.logout(function() {
        //console.log(data);
        alerts("info","Manual or Inactivity Logout success");
        sessionStorage.clear();
        $(login_modal_id).modal('show');
    });
}

function checkSession()
{

  if (typeof(Storage) !== "undefined")
  {
      if (sessionStorage.getItem("session_token") === null){
        $(login_modal_id).modal('show');
      }
      else {
        alerts("info","Page refreshed, session continued .. ");
        onLogin();
      }
  } else
    {
        alerts("error","No web Storage Support !");
    }
}

// login
$(document)
  .on('submit', '#l-form', function() {
  alerts("info","Please wait for authentication");
  $.regalartech.login($('#login_username').val(), $('#login_password').val() , loginHandle );
  return false;})
  .idle({onIdle: function(){removeToken();
  },
  idle: IDLE_TIMEOUT
  });


// logout
 $("#reg-out").click(function(){removeToken();});

 function submit_post_via_hidden_form(url, params) {
    var f = $("<form target='_blank' method='POST' style='display:none;'></form>").attr({
        action: url
    }).appendTo(document.body);

    for (var i in params) {
        if (params.hasOwnProperty(i)) {
            $('<input type="hidden" />').attr({
                name: i,
                value: params[i]
            }).appendTo(f);
        }
    }
    f.submit();
    f.remove();
}
