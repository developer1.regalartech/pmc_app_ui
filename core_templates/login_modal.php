<!--Modal: Login / Register Form-->
<div class="modal fade" id="regalar-login-register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false" style="background-color:white;">
    <div class="modal-dialog cascading-modal" role="document">
        <!--Content-->
        <div class="modal-content">

            <!--Modal cascading tabs-->
            <div class="modal-c-tabs">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs md-tabs tabs-2 light-blue darken-3" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#panel7" role="tab">  <h1>Pune Municipal Corporation</h1> </a>
                    </li>
                </ul>

                <!-- Tab panels -->
                <div class="tab-content">
                    <!--Panel 7-->
                    <div class="tab-pane fade in show active" id="panel7" role="tabpanel">

                        <!--Body-->
                        <div class="modal-body mb-1">
                          <form id="l-form">
                            <div class="md-form form-sm mb-5">
                                <i class="fa fa-envelope prefix"></i>
                                <input type="email" id="login_username" class="form-control form-control-sm validate" value = "aus_admin@aus.co.in" >
                                <label data-error="wrong" data-success="right" for="login_username">Your email</label>
                            </div>

                            <div class="md-form form-sm mb-4">
                                <i class="fa fa-lock prefix"></i>
                                <input type="password" id="login_password" class="form-control form-control-sm validate"value="aus@121" >
                                <label data-error="wrong" data-success="right" for="login_password">Your password</label>
                            </div>
                            <div class="text-center mt-2">
                                <button type="submit" class="btn btn-info"> Log in <i class="fa fa-sign-in ml-1" ></i></button>
                            </div>
                          </form>
                        </div>
                    </div>
                    <!--/.Panel 7-->

  
                </div>

            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: Login -->