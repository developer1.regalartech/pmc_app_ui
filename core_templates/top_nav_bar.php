<nav id="reg-nav" class="navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav">
    <!-- SideNav slide-out button -->
    <div class="float-left">
        <a href="#" data-activates="slide-out" class="button-collapse"><i class="fa fa-bars"></i></a>
    </div>
    <!-- Breadcrumb-->
    <div class="breadcrumb-dn mr-auto lead">
        <strong id="project-title"> </strong>
    </div>
    <ul class="nav navbar-nav nav-flex-icons ml-auto">
        <li class="nav-item dropdown">
            <a id="nav_user_name" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-user"></i> Profile </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">
                <a class="dropdown-item" href="#">Settings</a>
                <a class="dropdown-item" href="#"  onClick='removeToken();'>Log out</a>
            </div>
        </li>
    </ul>
</nav>

<script type="text/javascript" charset="utf8" >
(function() {

document.getElementById("nav_user_name").innerHTML =  sessionStorage.getItem('name');

})(); // all  must be inse herer ..


</script>