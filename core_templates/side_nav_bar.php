<div id="slide-out" class="side-nav sn-bg-4">
    <ul class="custom-scrollbar">
        <!-- Logo -->
        <li>
            <div class="logo-wrapper waves-light">
                <a href="/pmc_app_ui/modules/dashboard_admin/"><img
                        src="<?php echo ($GLOBALS['dir_root']); ?>/assets/log.png" class="img-fluid flex-center"></a>
            </div>
        </li>
        <!--/. Logo -->

        <!-- Side navigation links -->
        <li>
            <ul class="collapsible collapsible-accordion">
              
                <!--krishna GL -->
                <li><a class="collapsible-header waves-effect arrow-r" onclick="opensubmenu(this)"><i class="fa fa-hand-pointer-o"></i> Role
                        Management <i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/role_management/add_role.php"
                                    class="waves-effect"><i class="fa fa-plus"></i>  Add Role</a></li>

                        </ul>
                    </div>
                </li>
                <li><a class="collapsible-header waves-effect arrow-r" onclick="opensubmenu(this)"><i class="fa fa-hand-pointer-o"></i> User
                    Management <i class="fa fa-angle-down rotate-icon"></i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/user_mgmt/add_user.php"
                                class="waves-effect"><i class="fa fa-plus"></i>  Add User</a></li>

                    </ul>
                </div>
            </li>
            <li id="Reservation"><a class="collapsible-header waves-effect arrow-r"  onclick="opensubmenu(this)"><i class="fa fa-hand-pointer-o"></i>
                Reservations <i class="fa fa-angle-down rotate-icon"></i></a>
            <div class="collapsible-body">
              <ul>
                

                </ul>
            </div>
        </li> 
                <!-- <li id="DPcell"><a class="collapsible-header waves-effect arrow-r"  onclick="opensubmenu(this)"><i class="fa fa-hand-pointer-o"></i>
                        DP Cell <i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                      <ul>
                        

                        </ul>
                    </div>
                </li>  -->






                <li id="Section37"><a class="collapsible-header waves-effect arrow-r"  onclick="opensubmenu(this)"><i
                            class="fa fa-hand-pointer-o"></i> Section 37
                        <i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                            <!-- <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_37/mod_final_table.php"
                                    class="waves-effect"> Final Modification Table </a></li>
                            <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_37/mod_final.php"
                                    class="waves-effect">Final Modification Details</a></li>
                            <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_37/modificationdetails_table.php"
                                    class="waves-effect">Modification Details Table</a></li>
                            <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_37/modificationDetails.php"
                                    class="waves-effect"> Proposal Modification Details</a></li> -->

                        </ul>
                    </div>
                </li>


                <li id="Section15"><a class="collapsible-header waves-effect arrow-r"  onclick="opensubmenu(this)"><i
                            class="fa fa-hand-pointer-o"></i> Section
                        15.10 <i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                            <!-- <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_15_10/section15.10.php"
                                    class="waves-effect">Section 15.10 </a></li> -->

                        </ul>
                    </div>
                </li>

                <li id="Section49"><a class="collapsible-header waves-effect arrow-r"  onclick="opensubmenu(this)"><i
                            class="fa fa-hand-pointer-o"></i> Section 49
                        <i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                            <!-- <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_49/section49_1_table.php"
                                    class="waves-effect"> Section 49 1 Table </a></li>
                            <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_49/section49_1.php"
                                    class="waves-effect"> Section 49 1 Notice To State Govt </a></li>
                            <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_49/section49_2_table.php"
                                    class="waves-effect"> Section 49 2 Table </a></li>
                            <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_49/section49_2.php"
                                    class="waves-effect"> Section 49 2 Documents Related To Notice </a></li>
                            <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_49/section49_3_table.php"
                                    class="waves-effect"> Section 49 3 Table </a></li>
                            <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_49/section49_3.php"
                                    class="waves-effect"> Section 49 3 </a></li>
                            <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_49/section49_4_table.php"
                                    class="waves-effect"> Section 49 4 Table </a></li>
                            <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_49/section49_4.php"
                                    class="waves-effect"> Section 49 4 Govt. Decision </a></li>
                            <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_49/section49_5_table.php"
                                    class="waves-effect"> Section 49 5 Table </a></li>
                            <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_49/section49_5.php"
                                    class="waves-effect"> Section 49 5 </a></li>
                            <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_49/section49_7_table.php"
                                    class="waves-effect"> Section 49 7 Table </a></li>
                            <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_49/section49_7.php"
                                    class="waves-effect"> Section 49 7 Acquisition by PMC </a></li> -->
                        </ul>
                    </div>
                </li>

                <li id="Section49.1"><a class="collapsible-header waves-effect arrow-r"  onclick="opensubmenu(this)"><i
                            class="fa fa-hand-pointer-o"></i> Section 49.1
                        <i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                           

                        </ul>
                    </div>
                </li>
                <li id="Section49.2"><a class="collapsible-header waves-effect arrow-r"  onclick="opensubmenu(this)"><i
                            class="fa fa-hand-pointer-o"></i> Section 49.2
                        <i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                           

                        </ul>
                    </div>
                </li>
                <li id="Section49.3"><a class="collapsible-header waves-effect arrow-r"  onclick="opensubmenu(this)"><i
                            class="fa fa-hand-pointer-o"></i> Section 49.3
                        <i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                           

                        </ul>
                    </div>
                </li>
                <li id="Section49.4"><a class="collapsible-header waves-effect arrow-r"  onclick="opensubmenu(this)"><i
                            class="fa fa-hand-pointer-o"></i> Section 49.4
                        <i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                           

                        </ul>
                    </div>
                </li>
                <li id="Section49.5"><a class="collapsible-header waves-effect arrow-r"  onclick="opensubmenu(this)"><i
                            class="fa fa-hand-pointer-o"></i> Section 49.5
                        <i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                           

                        </ul>
                    </div>
                </li>
                <li id="Section49.7"><a class="collapsible-header waves-effect arrow-r"  onclick="opensubmenu(this)"><i
                            class="fa fa-hand-pointer-o"></i> Section 49.7
                        <i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                           

                        </ul>
                    </div>
                </li>

                <li id="Section50"><a class="collapsible-header waves-effect arrow-r"  onclick="opensubmenu(this)"><i
                            class="fa fa-hand-pointer-o"></i> Section 50
                        <i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                           

                        </ul>
                    </div>
                </li>


                <li id="Section51"><a class="collapsible-header waves-effect arrow-r"  onclick="opensubmenu(this)"><i
                            class="fa fa-hand-pointer-o"></i> Section 51
                        <i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                            <!-- <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_51/section51.php"
                                    class="waves-effect"> Section 51 </a></li> -->

                        </ul>
                    </div>
                </li>

                <li id="Section127"><a class="collapsible-header waves-effect arrow-r"  onclick="opensubmenu(this)"><i
                            class="fa fa-hand-pointer-o"></i> Section 127
                        <i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                            <!-- <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_127/section127_1.php"
                                    class="waves-effect"> Section 127 1 Enforcement Date </a></li>
                            <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_127/section127_3.php"
                                    class="waves-effect"> Section 127 3 Notice By Own </a></li>
                            <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/section_127/section127_4.php"
                                    class="waves-effect"> Section 127 4 Lapsing </a></li> -->

                        </ul>
                    </div>
                </li>

                <li id="netmodification"><a class="collapsible-header waves-effect arrow-r"  onclick="opensubmenu(this)"><i
                            class="fa fa-hand-pointer-o"></i> Net
                        Modification <i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                            <!-- <li><a href="<?php echo ($GLOBALS['dir_root']); ?>/modules/net_mod_wrt_area/NetModification_w_r_t_Area.php"
                                    class="waves-effect"> Net Modification With Respect To Area </a></li> -->


                        </ul>
                    </div>
                </li> 


                <!--raj -->


            </ul>
        </li>
        <!--/. Side navigation links -->
    </ul>
    <div class="sidenav-bg mask-strong"></div>
</div>
<script type="text/javascript">
function opensubmenu(li) {
    var ulDiv = li.parentElement.getElementsByClassName('collapsible-body')[0]
    if (ulDiv.style.display == 'none' || ulDiv.style.display == '' ){
        ulDiv.style.display = 'block'
    } else {
        ulDiv.style.display = 'none'
    }
}

</script>