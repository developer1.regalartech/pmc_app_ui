<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   
<script type="text/javascript" src="<?php echo ($GLOBALS['dir_root'].'/core'); ?>/js/popper.min.js"></script>

<script type="text/javascript" src="<?php echo ($GLOBALS['dir_root'].'/core'); ?>/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?php echo ($GLOBALS['dir_root'].'/core'); ?>/js/mdb.min.js"></script>

<script type="text/javascript" src="<?php echo ($GLOBALS['dir_root'].'/core'); ?>/reg-core/alerts.js"></script>

<script type="text/javascript" src="<?php echo ($GLOBALS['dir_root'].'/core'); ?>/reg-core/foundation.js"></script>

<script type="text/javascript" src="<?php echo ($GLOBALS['dir_root'].'/core'); ?>/reg-core/jwt-ui.js"></script>

<script type="text/javascript" src="<?php echo ($GLOBALS['dir_root'].'/core'); ?>/reg-core/initialize.js"></script>